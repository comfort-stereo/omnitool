import { missing } from "./predicate"
import { ErrorMessage } from "../../shared";

/**
 * Returns the input **value** if it is not null or undefined, else an error is thrown.
 */
export function unpack<T, O>(value: T | null | undefined): T

/**
 * Returns the input **value** if it is not null or undefined, else the **instead** value will be returned.
 */
export function unpack<T, O>(value: T | null | undefined, instead: O): T | O
export function unpack<T, O>(value: T | null | undefined, instead?: O): T | O {
    if (missing(value)) {
        if (instead === undefined) {
            throw new Error(ErrorMessage.unpackAssertion)
        }
        return instead
    }
    return value
}
