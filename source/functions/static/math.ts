import { Transform } from "../../types/types"
import { LoDash } from "../../shared";

/**
 * Returns the absolute value of a **number**.
 */
export function abs(number: number): number {
    return Math.abs(number)
}

/**
 * Rounds a **number** up to the nearest integer.
 */
export function ceil(number: number): number {
    return Math.ceil(number)
}

/**
 * Clamps a **number** between a **lower** and **upper** bound. If the **lower** bound is greater than the **upper**
 * bound **lower** will be swapped with **upper**.
 */
export function clamp(number: number, lower: number, upper: number): number {
    if (lower < upper) {
        return Math.min(Math.max(number, lower), upper)
    }
    return Math.min(Math.max(number, upper), lower)
}

/**
 * Rounds a **number** down to the nearest integer.
 */
export function floor(number: number): number {
    return Math.floor(number)
}

/**
 * Returns the greatest common divisor between two integers.
 */
export function gcd(number: number, other: number): number {
    number = Math.floor(number)
    other = Math.floor(other)
    if (number === 0 || other === 0) {
        return 1
    }
    if (number < other) {
        const temporary = other
        other = number
        number = temporary
    }
    return computeGCD(other, number)
}

function computeGCD(number: number, other: number): number {
    if (other === 0) {
        return Math.abs(number)
    }
    return computeGCD(other, number % other)
}

/**
 * Returns the maximum number of two numbers.
 */
export function high(first: number, second: number): number

/**
 * Returns the value for which a **transform** returns the maximum number.
 */
export function high<T>(first: T, second: T, transform: Transform<T, number>): T
export function high<T>(first: number | T, second: number | T, transform?: Transform<T, number>): number | T {
    if (transform) {
        return transform(first as T) > transform(second as T) ? first : second
    }
    return Math.max(first as number, second as number)
}

/**
 * Returns the minimum number of two numbers.
 */
export function low(first: number, second: number): number

/**
 * Returns the value for which a **transform** returns the minimum number.
 */
export function low<T>(first: T, second: T, transform: Transform<T, number>): T
export function low<T>(first: number | T, second: number | T, transform?: Transform<T, number>): number | T {
    if (transform) {
        return transform(first as T) < transform(second as T) ? first : second
    }
    return Math.min(first as number, second as number)
}

/**
 * Rounds a **number** to the nearest integer.
 *
 * *This function is implemented using LoDash's round().*
 */
export function round(number: number): number

/**
 * Rounds a **number** to a decimal position a given number of **places** away from the decimal point.
 *
 * *This function is implemented using LoDash's round().*
 */
export function round(number: number, places: number): number
export function round(number: number, places?: number): number {
    return LoDash.round(number, places)
}
