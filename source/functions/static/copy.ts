import { LoDash } from "../../shared";

/**
 * Returns a deep copy of an input **value**.
 *
 * *This function is implemented using LoDash's cloneDeep().*
 */
export function clone<T>(value: T): T {
    return LoDash.cloneDeep(value)
}

/**
 * Returns a shallow copy of an input **value**.
 *
 * *This function is implemented using LoDash's clone().*
 */
export function copy<T>(value: T): T {
    return LoDash.clone(value)
}
