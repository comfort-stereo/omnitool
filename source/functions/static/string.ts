import { Predicate } from "../../types/types"

/**
 * Returns true if all characters in a **string** are letters and there is at least one character.
 */
export function isAlpha(string: string): boolean {
    return Boolean(string) && allCharactersMatch(string, isAlphaCode)
}

/**
 * Returns true if all characters in a **string** are numeric digits and there is at least one character.
 */
export function isDigit(string: string): boolean {
    return Boolean(string) && allCharactersMatch(string, isDigitCode)
}

/**
 * Returns true if all letter characters in a **string** are lowercase and there is at least one character.
 */
export function isLower(string: string): boolean {
    return Boolean(string) && !anyCharactersMatch(string, isUpperCode)
}

/**
 * Returns true if all letter characters in a **string** are uppercase and there is at least one character.
 */
export function isUpper(string: string): boolean {
    return Boolean(string) && !anyCharactersMatch(string, isLowerCode)
}

const isBlankRegex = new RegExp("^\\s*$")

/**
 * Returns true if a **string** is empty or all characters in a **string** are whitespace.
 */
export function isBlank(string: string): boolean {
    return isBlankRegex.test(string)
}

/**
 * Converts a **string** to lowercase.
 */
export function lower(string: string): string {
    return string.toLowerCase()
}

/**
 * Evenly pads both sides of a **string** with spaces such that the resulting string has a specified **length**.
 * Returns the original string if it has a length greater than or equal to the specified **length**.
 */
export function pad(string: string, length: number): string

/**
 * Evenly pads both sides of a **string** with characters from **padding** such that the resulting string has a
 * specified **length**. Returns the original string if it has a length greater than or equal to the specified **length**.
 */
export function pad(string: string, length: number, padding: string): string
export function pad(string: string, length: number, padding: string = " "): string {
    length = Math.floor(length)
    if (string.length > length || padding.length === 0) {
        return string
    }
    const left: string[] = []
    const right: string[] = []

    let i = 0
    let currentLength = string.length

    while (currentLength++ < length) {
        left.push(padding[i % padding.length])
        if (currentLength >= length) {
            break
        }
        right.push(padding[i % padding.length])
        currentLength++
        i++
    }

    return left.join("") + string + right.join("")
}

/**
 * Pads the start of a **string** with spaces such that the resulting string has a specified **length**. Returns the
 * original string if it has a length greater than or equal to the specified **length**.
 */
export function padStart(string: string, length: number): string

/**
 * Pads the start of a **string** with characters from **padding** such that the resulting string has a specified
 * **length**. Returns the original string if it has a length greater than or equal to the specified **length**.
 */
export function padStart(string: string, length: number, padding: string): string
export function padStart(string: string, length: number, padding: string = " "): string {
    return padSide(string, Math.floor(length), padding, true)
}

/**
 * Pads the end of a **string** with spaces such that the resulting string has a specified **length**. Returns the
 * original string if it has a length greater than or equal to the specified **length**.
 */
export function padEnd(string: string, length: number): string

/**
 * Pads the start of a **string** with characters from **padding** such that the resulting string has a specified
 * **length**. Returns the original string if it has a length greater than or equal to the specified **length**.
 */
export function padEnd(string: string, length: number, padding: string): string
export function padEnd(string: string, length: number, padding: string = " "): string {
    return padSide(string, Math.floor(length), padding, false)
}

function padSide(string: string, length: number, characters: string, start: boolean): string {
    if (string.length > length || characters.length === 0) {
        return string
    }
    const side: string[] = []
    const size = string.length + length

    let i = 0
    let currentLength = string.length

    while (currentLength++ < length) {
        side.push(characters[i++ % characters.length])
    }

    const padding = side.join("")
    return start ? padding + string : string + padding
}

/**
 * Converts a **string** to uppercase.
 */
export function upper(string: string): string {
    return string.toUpperCase()
}

function isLowerCode(code: number) {
    return code >= 97 && code <= 122
}

function isUpperCode(code: number) {
    return code >= 65 && code <= 90
}

function isDigitCode(code: number) {
    return code >= 48 && code <= 57
}

function isAlphaCode(code: number) {
    return isLowerCode(code) || isUpperCode(code)
}

function allCharactersMatch(string: string, predicate: Predicate<number>): boolean {
    for (let i = 0; i < string.length; i++) {
        if (predicate(string.charCodeAt(i))) {
            continue
        }
        return false
    }
    return true
}

function anyCharactersMatch(string: string, predicate: Predicate<number>): boolean {
    for (let i = 0; i < string.length; i++) {
        if (predicate(string.charCodeAt(i))) {
            return true
        }
    }
    return false
}
