import { pad } from "./string"
import { Block } from "../../types/types"
import { ErrorMessage } from "../../shared";

/**
 * Throws an error with a default panic message.
 */
export function panic(): void

/**
 * Throws an error with a specified **message**.
 */
export function panic(message: string): void
export function panic(message?: string) {
    if (message === undefined) {
        message = ErrorMessage.defaultPanic
    }
    throw new Error(message)
}

/**
 * Immediately invokes a **block** callback. If the **block** throws an error the error will be returned. Returns
 * null otherwise.
 */
export function snag<T extends Error>(block: Block): T | null {
    try {
        block()
    } catch (error) {
        return error
    }
    return null
}
