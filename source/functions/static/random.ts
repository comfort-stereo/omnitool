import { ErrorMessage } from "../../shared";

/**
 * Returns true at a frequency determined by a given **probability**. A **probability** of one or higher guarantees a
 * return value of true and a **probability** of zero or less guarantees a return value of false.
 */
export function chance(probability: number): boolean {
    if (probability >= 1) {
        return true
    }
    if (probability <= 0) {
        return false
    }
    return Math.random() < probability
}

/**
 * Returns a pseudo-random floating point number between zero and one.
 */
export function random(): number

/**
 * Returns a pseudo-random floating point number between zero and an **upper** bound.
 */
export function random(upper: number): number

/**
 * Returns a pseudo-random floating point number between a **lower** and **upper** bound.
 */
export function random(lower: number, upper: number): number
export function random(first?: number, second?: number): number {
    if (first === undefined) {
        return Math.random()
    }
    let lower, upper
    if (second === undefined) {
        lower = 0
        upper = first
        if (lower > upper) {
            throw new Error(ErrorMessage.invalidRandomBoundsZero)
        }
    } else {
        lower = first
        upper = second
        if (lower > upper) {
            throw new Error(ErrorMessage.invalidRandomBounds)
        }
    }
    return Math.random() * (upper - lower) + lower
}

/**
 * Returns a pseudo-random integer between zero and an **upper** bound.
 */
export function randomInteger(upper: number): number

/**
 * Returns a pseudo-random integer between a **lower** and **upper** bound.
 */
export function randomInteger(lower: number, upper: number): number
export function randomInteger(first: number, second?: number): number {
    first = Math.floor(first)
    if (second !== undefined) {
        second = Math.floor(second)
    }
    return Math.round(random(first, second as number))
}
