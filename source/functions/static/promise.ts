const immediateDelayPromise = new Promise<void>((resolve) => resolve(undefined))

/**
 * Returns a promise that resolves after zero milliseconds.
 */
export function delay(): Promise<void>

/**
 * Returns a promise that resolves after a given number of milliseconds.
 */
export function delay(ms: number): Promise<void>
export function delay(ms: number = 0): Promise<void> {
    if (ms > 0) {
        return new Promise((resolve) => setTimeout(resolve, ms))
    }
    return immediateDelayPromise
}
