import { Block } from "../../types/types"

/**
 * Immediately invokes a **block** callback and returns the time in milliseconds it takes to execute.
 */
export function duration(block: Block): number {
    const start = Date.now()
    block()
    return Date.now() - start
}

/**
 * Returns the number of elapsed milliseconds since the UNIX epoch.
 */
export function now(): number {
    return Date.now()
}

/**
 * Blocks the current thread for a given number of milliseconds.
 */
export function sleep(ms: number) {
    const start = Date.now()
    while (Date.now() - start < ms) {
    }
}
