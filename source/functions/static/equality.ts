import { Predicate } from "../../types/types"
import { LoDash } from "../../shared";

/**
 * Returns true if two values are deeply structurally equal.
 *
 * *This function is implemented using LoDash's isEqual().*
 */
export function equal<T>(value: T, other: T): boolean

/**
 * Returns a predicate function that returns true if an input value is deeply structurally equal to the original
 * **value**.
 *
 * *This function is implemented using LoDash's isEqual().*
 */
export function equal<T>(value: T): Predicate<T>
export function equal<T>(value: T, other?: T): boolean | Predicate<T> {
    if (other === undefined) {
        return (other: T) => LoDash.isEqual(value, other)
    }
    return LoDash.isEqual(value, other)
}

/**
 * Returns true if two values are not deeply structurally equal.
 *
 * *This function is implemented using LoDash's isEqual().*
 */
export function unequal<T>(value: T, other: T): boolean

/**
 * Returns a predicate function that returns true if an input value is not deeply structurally equal to the original
 * **value**.
 *
 * *This function is implemented using LoDash's isEqual().*
 */
export function unequal<T>(value: T): Predicate<T>
export function unequal<T>(value: T, other?: T): boolean | Predicate<T> {
    if (other === undefined) {
        return (other: T) => !LoDash.isEqual(value, other)
    }
    return !LoDash.isEqual(value, other)
}

/**
 * Returns true if two values are strictly equal.
 */
export function is<T>(value: T, other: T): boolean

/**
 * Returns a predicate function that returns true if an input value is strictly equal to the original **value**.
 */
export function is<T>(value: T): Predicate<T>
export function is<T>(value: T, other?: T): boolean | Predicate<T> {
    if (other === undefined) {
        return (other) => other === value
    }
    return value === other
}

/**
 * Returns true if two values are not strictly equal.
 */
export function isnt<T>(value: T, other: T): boolean

/**
 * Returns a predicate function that returns true if an input value is not strictly equal to the original **value**.
 */
export function isnt<T>(value: T): Predicate<T>
export function isnt<T>(value: T, other?: T): boolean | Predicate<T> {
    if (other === undefined) {
        return (other) => other !== value
    }
    return value !== other
}
