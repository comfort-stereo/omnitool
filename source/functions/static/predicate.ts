import { Primitive } from "../../types/types"

/**
 * Returns true if a **value** is not null or undefined.
 */
export function exists<T>(value: T | undefined | null): value is T {
    return !missing(value)
}

/**
 * Returns true if a **value** is an array.
 */
export function isArray<T>(value: any): value is any[] {
    return Array.isArray(value)
}

/**
 * Returns true if a **value** is a boolean primitive. Does *not* return true for wrapper objects.
 */
export function isBoolean(value: any): value is boolean {
    return value === true || value === false
}

/**
 * Returns true if a **value** is not undefined.
 */
export function isDefined<T>(value: T | undefined): value is T {
    return value !== undefined
}

/**
 * Returns true if a **value** is an error.
 */
export function isError(value: any): value is Error {
    /* TODO: This should be changed to handle checking across contexts. */
    return value instanceof Error
}

/**
 * Returns true if a **value** is considered a function.
 */
export function isFunction(value: any): value is Function {
    return typeof value === "function"
}

/**
 * Returns true if a **value** is null.
 */
export function isNull(value: any): value is null {
    return value === null
}

/**
 * Returns true if a **value** is a number primitive. Does *not* return true for wrapper objects.
 */
export function isNumber(value: any): value is number {
    return typeof value === "number"
}

/**
 * Returns true if a **value** is not null.
 */
export function isntNull<T>(value: T | null): value is T {
    return value !== null
}

/**
 * Returns true if a **value** is an object.
 */
export function isObject(value: any): value is object {
    return value === Object(value)
}

/**
 * Returns true if a **value** is a primitive. Does *not* return true for wrapper objects.
 */
export function isPrimitive(value: any): value is Primitive {
    return isUndefined(value)
        || isNull(value)
        || isBoolean(value)
        || isNumber(value)
        || isString(value)
        || isSymbol(value)
}

/**
 * Returns true if a **value** is a regular expression object.
 */
export function isRegularExpression(value: any): value is RegExp {
    return Object.prototype.toString.call(value) === "[object RegExp]"
}

/**
 * Returns true if a **value** is a string primitive. Does *not* return true for wrapper objects.
 */
export function isString(value: any): value is string {
    return typeof value === "string"
}

/**
 * Returns true if a **value** is a symbol primitive. Does *not* return true for wrapper objects.
 */
export function isSymbol(value: any): value is Symbol {
    return typeof value === "symbol"
}

/**
 * Returns true if a **value** is undefined.
 */
export function isUndefined(value: any): value is undefined {
    return value === undefined
}

/**
 * Returns true if a **value** is null or undefined.
 */
export function missing<T>(value: T | undefined | null): value is null | undefined {
    return value === undefined || value === null
}
