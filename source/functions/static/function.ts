import { Action, Block, Producer } from "../../types/types"

/**
 * Invokes an **action** on a **value** and returns the original **value**.
 */
export function apply<T>(value: T, action: Action<T>): T {
    action(value)
    return value
}

/**
 * Returns the input **value**.
 */
export function identity<T>(value: T): T {
    return value
}

/**
 * Returns a wrapper function over an input function which produces a number. The wrapper function is
 * identical to the original function but always returns the negative of the original's return value.
 */
export function negate<T extends (...args: any[]) => number>(fn: T): T {
    return ((...args) => -fn(...args)) as T
}

/**
 * Returns false.
 */
export function no(): false {
    return false
}

/**
 * Takes an arbitrary number of arguments of any type, does nothing and returns undefined.
 */
export function noop(...args: any[]): undefined {
    return undefined
}

/**
 * Returns a wrapper function over a **predicate** that returns true when the predicate is false and false when the
 * predicate it true.
 */
export function not<T extends (...args: any[]) => boolean>(predicate: T): T {
    return ((...args) => !predicate(...args)) as T
}

/**
 * Immediately invokes a callback and returns the result.
 */
export function run<T>(producer: Producer<T>): T

/**
 * Immediately invokes a callback.
 */
export function run(block: Block): void
export function run<T>(callback: Producer<T>): T {
    return callback()
}

/**
 * Invokes a **callback** function **count** times. The **callback** is passed the current index of the loop and an
 * **end** callback. If **end** is invoked the loop will exit.
 */
export function times(count: number, callback: (index: number, end: () => void) => void) {
    count = Math.floor(count)

    let ended = false
    const end = () => {
        ended = true
    }

    for (let i = 0; i < count; i++) {
        callback(i, end)
        if (ended) {
            break
        }
    }
}

/**
 * Returns true.
 */
export function yes(): true {
    return true
}
