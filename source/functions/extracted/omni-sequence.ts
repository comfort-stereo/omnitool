import { OmniSequence, OutDestination } from "../../types/sequence/omni-sequence"
import { Pipe } from "../../types/sequence/pipe"
import { Span } from "../../types/sequence/span"
import { Comparator, Dictionary, PairPredicate, Positional, Predicate, Sequence, Transform, Falsy } from "../../types/types"
import { is } from "../static/equality"

/**
 * Pipe used when a pipe is only temporarily needed.
 */
const recycledPipe = new Pipe<any>([])

/**
 * Span used when a span is only temporarily needed.
 */
const recycledSpan = new Span<any>([], 0, 0)

/**
 * Allocates a new pipe over a **sequence**.
 */
function getPipe<T>(sequence: Sequence<T>): OmniSequence<T> {
    return new Pipe(sequence)
}

/**
 * Allocates a new span over a positional **sequence**.
 */
function getSpan<T>(sequence: Positional<T>): OmniSequence<T> {
    return new Span(sequence)
}

/**
 * Re-seats the module's recycled pipe onto a **sequence**.
 */
function resetPipe<T>(sequence: Sequence<T>): OmniSequence<T> {
    (recycledPipe as any)._seat(sequence)
    return recycledPipe
}

/**
 * Re-seats the module's recycled span onto a positional **sequence**.
 */
function resetSpan<T>(sequence: Positional<T>): OmniSequence<T> {
    (recycledSpan as any)._seat(sequence)
    return recycledSpan
}

/**
 * Returns true if the **sequence** can be considered positional and a span can be created from it.
 */
function isPositional<T>(sequence: Sequence<T>): boolean {
    return Array.isArray(sequence) || typeof sequence === "string"
}

/**
 * Returns a new span over the **sequence** if the input **sequence** is positional and a new pipe otherwise.
 */
function getOmniSequence<T>(sequence: Sequence<T>): OmniSequence<T> {
    if (sequence instanceof Span || sequence instanceof Pipe) {
        return sequence
    }
    return isPositional(sequence) ? getSpan(sequence as Positional<T>) : getPipe(sequence)
}

/**
 * Returns a recycled span over the **sequence** if the input **sequence** is positional and a recycled pipe otherwise.
 */
function resetOmniSequence<T>(sequence: Sequence<T>): OmniSequence<T> {
    if (sequence instanceof Span || sequence instanceof Pipe) {
        return sequence
    }
    return isPositional(sequence) ? resetSpan(sequence as Positional<T>) : resetPipe(sequence)
}

/**
 * Static version of [[OmniSequence.solid]] that works on any sequence.
 */
export function solid<T>(sequence: Sequence<T>): OmniSequence<T> {
    return resetOmniSequence(sequence).solid()
}

/**
 * Static version of [[OmniSequence.all]] that works on any sequence.
 */
export function all<T>(sequence: Sequence<T>, predicate: Predicate<T>): boolean {
    return resetOmniSequence(sequence).all(predicate)
}

/**
 * Static version of [[OmniSequence.as]] that works on any sequence.
 */
export function as<T, O extends T>(sequence: Sequence<T>): OmniSequence<O> {
    return getOmniSequence(sequence).as<O>()
}

/**
 * Static version of [[OmniSequence.concat]] that works on any sequence.
 */
export function concat<T>(sequence: Sequence<T>, other: Sequence<T>): OmniSequence<T> {
    return getOmniSequence(sequence).concat(other)
}

/**
 * Static version of [[OmniSequence.append]] that works on any sequence.
 */
export function append<T>(sequence: Sequence<T>, ...elements: T[]): OmniSequence<T> {
    return getOmniSequence(sequence).append(...elements)
}

/**
 * Static version of [[OmniSequence.associate]] that works on any sequence.
 */
export function associate<T, K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<[K, T]> {
    return getOmniSequence(sequence).associate(transform)
}

/**
 * Static version of [[OmniSequence.at]] that works on any sequence.
 */
export function at<T>(sequence: Sequence<T>, index: number): T {
    return resetOmniSequence(sequence).at(index)
}

/**
 * Static version of [[OmniSequence.cleave]] that works on any sequence.
 */
export function cleave<T>(sequence: Sequence<T>, index: number): [T[], T[]] {
    return resetOmniSequence(sequence).cleave(index)
}

/**
 * Static version of [[OmniSequence.count]] that works on any sequence.
 */
export function count<T>(sequence: Sequence<T>): number
export function count<T>(sequence: Sequence<T>, predicate: Predicate<T>): number
export function count<T>(sequence: Sequence<T>, predicate?: Predicate<T>): number {
    return resetOmniSequence(sequence).count(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.cut]] that works on any sequence.
 */
export function cut<T>(sequence: Sequence<T>, start: number): OmniSequence<T>
export function cut<T>(sequence: Sequence<T>, start: number, count: number): OmniSequence<T>
export function cut<T>(sequence: Sequence<T>, start: number, count?: number): OmniSequence<T> {
    return getOmniSequence(sequence).cut(start, count as number)
}

/**
 * Static version of [[OmniSequence.chunk]] that works on any sequence.
 */
export function chunk<T>(sequence: Sequence<T>, count: number): OmniSequence<T[]> {
    return getOmniSequence(sequence).chunk(count)
}

/**
 * Static version of [[OmniSequence.difference]] that works on any sequence.
 */
export function difference<T, K>(sequence: Sequence<T>, other: Sequence<T>): OmniSequence<T>
export function difference<T, K>(sequence: Sequence<T>, other: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
export function difference<T, K>(sequence: Sequence<T>, other: Sequence<T>, 
                                 transform?: Transform<T, K>): OmniSequence<T> {
    return getOmniSequence(sequence).difference(other, transform as Transform<T, K>)
}

/**
 * Static version of [[OmniSequence.done]] that works on any sequence.
 */
export function done<T>(sequence: Sequence<T>): T[] {
    return resetOmniSequence(sequence).done()
}

/**
 * Static version of [[OmniSequence.drop]] that works on any sequence.
 */
export function drop<T>(sequence: Sequence<T>): OmniSequence<T>
export function drop<T>(sequence: Sequence<T>, count: number): OmniSequence<T>
export function drop<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<T>
export function drop<T>(sequence: Sequence<T>, argument?: number | Predicate<T>): OmniSequence<T> {
    return getOmniSequence(sequence).drop(argument as any)
}

/**
 * Static version of [[OmniSequence.dropLast]] that works on any sequence.
 */
export function dropLast<T>(sequence: Sequence<T>): OmniSequence<T>
export function dropLast<T>(sequence: Sequence<T>, count: number): OmniSequence<T>
export function dropLast<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<T>
export function dropLast<T>(sequence: Sequence<T>, argument?: number | Predicate<T>): OmniSequence<T> {
    return getOmniSequence(sequence).dropLast(argument as any)
}

/**
 * Static version of [[OmniSequence.each]] that works on any sequence.
 */
export function each<T>(sequence: Sequence<T>,
                        action: (element: T, index: number, end: () => void) => void): OmniSequence<T> {
    return getOmniSequence(sequence).each(action)
}

/**
 * Static version of [[OmniSequence.endsWith]] that works on any sequence.
 */
export function endsWith<T>(sequence: Sequence<T>, suffix: Positional<T>): boolean
export function endsWith<T>(sequence: Sequence<T>, suffix: Positional<T>,
                            equals: (value: T, other: T) => boolean): boolean
export function endsWith<T>(sequence: Sequence<T>, suffix: Positional<T>,
                            equals: (value: T, other: T) => boolean = is): boolean {
    return resetOmniSequence(sequence).endsWith(suffix, equals)
}

/**
 * Static version of [[OmniSequence.entries]] that works on any sequence.
 */
export function entries<T>(sequence: Sequence<T>): OmniSequence<[number, T]> {
    return getOmniSequence(sequence).entries()
}

/**
 * Static version of [[OmniSequence.entriesReversed]] that works on any sequence.
 */
export function entriesReversed<T>(sequence: Sequence<T>): OmniSequence<[number, T]> {
    return getOmniSequence(sequence).entriesReversed()
}

/**
 * Static version of [[OmniSequence.filter]] that works on any sequence.
 */
export function filter<T>(sequence: Sequence<T>): OmniSequence<T>
export function filter<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<T>
export function filter<T>(sequence: Sequence<T>, predicate?: Predicate<T>): OmniSequence<T> {
    return getOmniSequence(sequence).filter(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.find]] that works on any sequence.
 */
export function find<T>(sequence: Sequence<T>, predicate: Predicate<T>): T | null
export function find<T, O>(sequence: Sequence<T>, predicate: Predicate<T>, instead: O): T | O
export function find<T, O>(sequence: Sequence<T>, predicate: Predicate<T>, instead?: O): T | O {
    return resetOmniSequence(sequence).find(predicate, instead) as T | O
}

/**
 * Static version of [[OmniSequence.findLast]] that works on any sequence.
 */
export function findLast<T>(sequence: Sequence<T>, predicate: Predicate<T>): T
export function findLast<T, O>(sequence: Sequence<T>, predicate: Predicate<T>, instead: O): T | O
export function findLast<T, O>(sequence: Sequence<T>, predicate: Predicate<T>, instead?: O): T | O {
    return resetOmniSequence(sequence).findLast(predicate, instead) as T | O
}

/**
 * Static version of [[OmniSequence.first]] that works on any sequence.
 */
export function first<L, R>(pair: [L, R]): L
export function first<T, O>(sequence: Sequence<T>): T
export function first<T, O>(sequence: Sequence<T>, instead: O): T | O
export function first<T, O>(sequence: Sequence<T>, instead?: O): T | O {
    return resetOmniSequence(sequence).first(instead) as T | O
}

/**
 * Static version of [[OmniSequence.flat]] that works on any sequence.
 */
export function flat<T>(sequence: Sequence<Sequence<T>>): OmniSequence<T> {
    return getOmniSequence(sequence).flat()
}

/**
 * Static version of [[OmniSequence.fold]] that works on any sequence.
 */
export function fold<T, R>(sequence: Sequence<T>, initial: R, reducer: (accumulator: R, current: T) => R): R {
    return resetOmniSequence(sequence).fold(initial, reducer)
}

/**
 * Static version of [[OmniSequence.index]] that works on any sequence.
 */
export function index<T>(sequence: Sequence<T>, predicate: Predicate<T>): number {
    return resetOmniSequence(sequence).index(predicate)
}

/**
 * Static version of [[OmniSequence.indices]] that works on any sequence.
 */
export function indices<T>(sequence: Sequence<T>): OmniSequence<number>
export function indices<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<number>
export function indices<T>(sequence: Sequence<T>, predicate?: Predicate<T>): OmniSequence<number> {
    return getOmniSequence(sequence).indices(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.indicesReversed]] that works on any sequence.
 */
export function indicesReversed<T>(sequence: Sequence<T>): OmniSequence<number>
export function indicesReversed<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<number>
export function indicesReversed<T>(sequence: Sequence<T>, predicate?: Predicate<T>): OmniSequence<number> {
    return getOmniSequence(sequence).indicesReversed(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.inject]] that works on any sequence.
 */
export function inject<T>(sequence: Sequence<T>, index: number, elements: Sequence<T>): OmniSequence<T> {
    return getOmniSequence(sequence).inject(index, elements)
}

/**
 * Static version of [[OmniSequence.intersect]] that works on any sequence.
 */
export function intersect<T, K>(sequence: Sequence<T>, other: Sequence<T>): OmniSequence<T>
export function intersect<T, K>(sequence: Sequence<T>, other: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
export function intersect<T, K>(sequence: Sequence<T>, other: Sequence<T>, 
                                transform?: Transform<T, K>): OmniSequence<T> {
    return getOmniSequence(sequence).intersect(other, transform as Transform<T, K>)
}

/**
 * Static version of [[OmniSequence.insert]] that works on any sequence.
 */
export function insert<T>(sequence: Sequence<T>, index: number, ...elements: T[]): OmniSequence<T> {
    return getOmniSequence(sequence).insert(index, ...elements)
}

/**
 * Static version of [[OmniSequence.invert]] that works on any sequence.
 */
export function invert<K, V>(sequence: Sequence<[K, V]>): OmniSequence<[V, K]> {
    return getOmniSequence(sequence).invert()
}

/**
 * Static version of [[OmniSequence.join]] that works on any sequence.
 */
export function join<T>(sequence: Sequence<T>): string
export function join<T>(sequence: Sequence<T>, separator: string): string
export function join<T>(sequence: Sequence<T>, separator: string = ""): string {
    return resetOmniSequence(sequence).join(separator)
}

/**
 * Static version of [[OmniSequence.last]] that works on any sequence.
 */
export function last<L, R>(pair: [L, R]): R
export function last<T, O>(sequence: Sequence<T>): T
export function last<T, O>(sequence: Sequence<T>, instead: O): T | O
export function last<T, O>(sequence: Sequence<T>, instead?: O): T | O {
    return resetOmniSequence(sequence).last(instead) as T | O
}

/**
 * Static version of [[OmniSequence.lastIndex]] that works on any sequence.
 */
export function lastIndex<T>(sequence: Sequence<T>): number
export function lastIndex<T>(sequence: Sequence<T>, predicate: Predicate<T>): number
export function lastIndex<T>(sequence: Sequence<T>, predicate?: Predicate<T>): number {
    return resetOmniSequence(sequence).lastIndex(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.map]] that works on any sequence.
 */
export function map<T, OK, OV>(sequence: Sequence<T>, transform: Transform<T, [OK, OV]>): OmniSequence<[OK, OV]>
export function map<T, O>(sequence: Sequence<T>, transform: Transform<T, O>): OmniSequence<O>
export function map<T, O>(sequence: Sequence<T>, transform: Transform<T, O>): OmniSequence<O> {
    return getOmniSequence(sequence).map(transform)
}

/**
 * Static version of [[OmniSequence.max]] that works on any sequence.
 */
export function max(sequence: Sequence<number>): number
export function max<T>(sequence: Sequence<T>, transform: Transform<T, number>): T
export function max<T>(sequence: Sequence<T>, transform?: Transform<T, number>): T | number {
    return resetOmniSequence(sequence).max(transform as Transform<T, number>)
}

/**
 * Static version of [[OmniSequence.mean]] that works on any sequence.
 */
export function mean(sequence: Sequence<number>): number {
    return resetOmniSequence(sequence).mean()
}

/**
 * Static version of [[OmniSequence.min]] that works on any sequence.
 */
export function min(sequence: Sequence<number>): number
export function min<T>(sequence: Sequence<T>, transform: Transform<T, number>): T
export function min<T>(sequence: Sequence<T>, transform?: Transform<T, number>): T | number {
    return resetOmniSequence(sequence).min(transform as Transform<T, number>)
}

/**
 * Static version of [[OmniSequence.none]] that works on any sequence.
 */
export function none<T>(sequence: Sequence<T>): boolean
export function none<T>(sequence: Sequence<T>, predicate: Predicate<T>): boolean
export function none<T>(sequence: Sequence<T>, predicate?: Predicate<T>): boolean {
    return resetOmniSequence(sequence).none(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.partition]] that works on any sequence.
 */
export function partition<T>(sequence: Sequence<T>, predicate: Predicate<T>): [T[], T[]] {
    return resetOmniSequence(sequence).partition(predicate)
}

/**
 * Static version of [[OmniSequence.permute]] that works on any sequence.
 */
export function permute<T>(sequence: Sequence<T>): OmniSequence<T[]> {
    return resetOmniSequence(sequence).permute()
}

/**
 * Static version of [[OmniSequence.prepend]] that works on any sequence.
 */
export function prepend<T>(sequence: Sequence<T>, ...elements: T[]): OmniSequence<T> {
    return getOmniSequence(sequence).prepend(...elements)
}

/**
 * Static version of [[OmniSequence.rank]] that works on any sequence.
 */
export function rank<T>(sequence: Sequence<T>, transform: Transform<T, number>): OmniSequence<T> {
    return getOmniSequence(sequence).rank(transform)
}

/**
 * Static version of [[OmniSequence.remove]] that works on any sequence.
 */
export function remove<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<T>
export function remove<T>(sequence: Sequence<T>, predicate: Predicate<T>, count: number): OmniSequence<T>
export function remove<T>(sequence: Sequence<T>, predicate: Predicate<T>, count?: number): OmniSequence<T> {
    return getOmniSequence(sequence).remove(predicate, count as number)
}

/**
 * Static version of [[OmniSequence.removeLast]] that works on any sequence.
 */
export function removeLast<T>(sequence: Sequence<T>, predicate: Predicate<T>, count: number): OmniSequence<T> {
    return getOmniSequence(sequence).removeLast(predicate, count)
}

/**
 * Static version of [[OmniSequence.repeat]] that works on any sequence.
 */
export function repeat<T>(sequence: Sequence<T>): OmniSequence<T>
export function repeat<T>(sequence: Sequence<T>, count: number): OmniSequence<T>
export function repeat<T>(sequence: Sequence<T>, count?: number): OmniSequence<T> {
    return getOmniSequence(sequence).repeat(count as number)
}

/**
 * Static version of [[OmniSequence.replace]] that works on any sequence.
 */
export function replace<T, R>(sequence: Sequence<T>, predicate: Predicate<T>, replacement: R): OmniSequence<T | R>
export function replace<T, R>(sequence: Sequence<T>, predicate: Predicate<T>, replacement: R,
                              count: number): OmniSequence<T | R>
export function replace<T, R>(sequence: Sequence<T>, predicate: Predicate<T>, replacement: R,
                              count?: number): OmniSequence<T | R> {
    return getOmniSequence(sequence).replace(predicate, replacement, count as number)
}

/**
 * Static version of [[OmniSequence.replaceLast]] that works on any sequence.
 */
export function replaceLast<T, R>(sequence: Sequence<T>, predicate: Predicate<T>, replacement: R,
                                  count: number): OmniSequence<T | R> {
    return getOmniSequence(sequence).replaceLast(predicate, replacement, count)
}

/**
 * Static version of [[OmniSequence.reversed]] that works on any sequence.
 */
export function reversed<T>(sequence: Sequence<T>): OmniSequence<T> {
    return getOmniSequence(sequence).reversed()
}

/**
 * Static version of [[OmniSequence.set]] that works on any sequence.
 */
export function set<T>(sequence: Sequence<T>, index: number, element: T): OmniSequence<T> {
    return getOmniSequence(sequence).set(index, element)
}

/**
 * Static version of [[OmniSequence.slice]] that works on any sequence.
 */
export function slice<T>(sequence: Sequence<T>, start: number, end: number): OmniSequence<T> {
    return getOmniSequence(sequence).slice(start, end as number)
}

/**
 * Static version of [[OmniSequence.some]] that works on any sequence.
 */
export function some<T>(sequence: Sequence<T>): boolean
export function some<T>(sequence: Sequence<T>, predicate: Predicate<T>): boolean
export function some<T>(sequence: Sequence<T>, predicate?: Predicate<T>): boolean {
    return resetOmniSequence(sequence).some(predicate as Predicate<T>)
}

/**
 * Static version of [[OmniSequence.sort]] that works on any sequence.
 */
export function sort(sequence: Sequence<number>): OmniSequence<number>
export function sort(sequence: Sequence<number>, descending: boolean): OmniSequence<number>

export function sort(sequence: Sequence<string>): OmniSequence<string>
export function sort(sequence: Sequence<string>, descending: boolean): OmniSequence<string>

export function sort<T>(sequence: Sequence<T>, comparator: Comparator<T>): OmniSequence<T>

export function sort<T>(sequence: Sequence<T | number | string>,
                        second?: Comparator<T> | boolean): OmniSequence<T | number | string> {
    return getOmniSequence(sequence).sort(second as any)
}

/**
 * Static version of [[OmniSequence.startsWith]] that works on any sequence.
 */
export function startsWith<T>(sequence: Sequence<T>, prefix: Sequence<T>): boolean
export function startsWith<T>(sequence: Sequence<T>, prefix: Sequence<T>, equals: PairPredicate<T>): boolean
export function startsWith<T>(sequence: Sequence<T>, prefix: Sequence<T>, equals?: PairPredicate<T>): boolean {
    return resetOmniSequence(sequence).startsWith(prefix, equals as PairPredicate<T>)
}

/**
 * Static version of [[OmniSequence.sum]] that works on any sequence.
 */
export function sum(sequence: Sequence<number>): number {
    return resetOmniSequence(sequence).sum()
}

/**
 * Static version of [[OmniSequence.swap]] that works on any sequence.
 */
export function swap<T>(sequence: Sequence<T>, first: number, second: number): OmniSequence<T> {
    return getOmniSequence(sequence).swap(first, second)
}

/**
 * Static version of [[OmniSequence.take]] that works on any sequence.
 */
export function take<T>(sequence: Sequence<T>): OmniSequence<T>
export function take<T>(sequence: Sequence<T>, count: number): OmniSequence<T>
export function take<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<T>
export function take<T>(sequence: Sequence<T>, argument?: number | Predicate<T>): OmniSequence<T> {
    return getOmniSequence(sequence).take(argument as any)
}

/**
 * Static version of [[OmniSequence.takeLast]] that works on any sequence.
 */
export function takeLast<T>(sequence: Sequence<T>, count: number): OmniSequence<T>
export function takeLast<T>(sequence: Sequence<T>, predicate: Predicate<T>): OmniSequence<T>
export function takeLast<T>(sequence: Sequence<T>, argument: number | Predicate<T>): OmniSequence<T> {
    return getOmniSequence(sequence).takeLast(argument as any)
}

/**
 * Static version of [[OmniSequence.toArray]] that works on any sequence.
 */
export function toArray<T>(sequence: Sequence<T>): T[]
export function toArray<T>(sequence: Sequence<T>, destination: T[]): T[]
export function toArray<T>(sequence: Sequence<T>, destination: T[], clear: boolean): T[]
export function toArray<T>(sequence: Sequence<T>, destination?: T[], clear?: boolean): T[] {
    return resetOmniSequence(sequence).toArray(destination as T[], clear as boolean)
}

/**
 * Static version of [[OmniSequence.toMap]] that works on any sequence.
 */
export function toMap<K, V>(sequence: Sequence<[K, V]>): Map<K, V>
export function toMap<K, V>(sequence: Sequence<[K, V]>, destination: Map<K, V>): Map<K, V>
export function toMap<K, V>(sequence: Sequence<[K, V]>, destination: Map<K, V>, clear: boolean): Map<K, V>
export function toMap<K, V>(sequence: Sequence<[K, V]>, destination?: Map<K, V>, clear: boolean = false): Map<K, V> {
    return resetOmniSequence(sequence).toMap(destination as Map<K, V>, clear as boolean)
}

/**
 * Static version of [[OmniSequence.toObject]] that works on any sequence.
 */
export function toObject<T>(sequence: Sequence<[string, T]>): Dictionary<T>
export function toObject<T>(sequence: Sequence<[string, T]>, destination: Dictionary<T>): Dictionary<T>
export function toObject<T>(sequence: Sequence<[string, T]>, destination: Dictionary<T>, clear: boolean): Dictionary<T>
export function toObject<T>(sequence: Sequence<[string, T]>, destination?: Dictionary<T>, clear: boolean = false): Dictionary<T> {
    return resetOmniSequence(sequence).toObject(destination as Dictionary<T>, clear as boolean)
}

/**
 * Static version of [[OmniSequence.toSet]] that works on any sequence.
 */
export function toSet<T>(sequence: Sequence<T>): Set<T>
export function toSet<T>(sequence: Sequence<T>, destination: Set<T>): Set<T>
export function toSet<T>(sequence: Sequence<T>, destination: Set<T>, clear: boolean): Set<T>
export function toSet<T>(sequence: Sequence<T>, destination?: Set<T>, clear?: boolean): Set<T> {
    return resetOmniSequence(sequence).toSet(destination as Set<T>, clear as boolean)
}

/**
 * Static version of [[OmniSequence.union]] that works on any sequence.
 */
export function union<T, K>(sequence: Sequence<T>, other: Sequence<T>): OmniSequence<T>
export function union<T, K>(sequence: Sequence<T>, other: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
export function union<T, K>(sequence: Sequence<T>, other: Sequence<T>, 
                            transform?: Transform<T, K>): OmniSequence<T> {
    return getOmniSequence(sequence).union(other, transform as Transform<T, K>)
}

/**
 * Static version of [[OmniSequence.unique]] that works on any sequence.
 */
export function unique<T, K>(sequence: Sequence<T>): OmniSequence<T>
export function unique<T, K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
export function unique<T, K>(sequence: Sequence<T>, transform?: Transform<T, K>): OmniSequence<T> {
    return getOmniSequence(sequence).unique(transform as Transform<T, K>)
}

/**
 * Static version of [[OmniSequence.zip]] that works on any sequence.
 */
export function zip<T, V>(sequence: Sequence<T>, other: Sequence<V>): OmniSequence<[T, V]> {
    return getOmniSequence(sequence).zip(other)
}
