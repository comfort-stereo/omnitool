/**
 * @hidden
 */
export const isGreaterThan = (value: number, other: number) => value > other

/**
 * @hidden
 */
export const isLessThan = (value: number, other: number) => value < other

/**
 * @hidden
 */
export namespace ErrorMessage {
    export const findElementNotFound = "[find()]: Element was not found."
    export const indexOutOfBoundsAt = "[at()]: Index out of bounds."
    export const indexOutOfBoundsCleave = "[cleave()]: Index out of bounds."
    export const indexOutOfBoundsCut = "[cut()]: Index out of bounds."
    export const indexOutOfBoundsInject = "[inject()]: Index out of bounds."
    export const indexOutOfBoundsInsert = "[insert()]: Index out of bounds."
    export const indexOutOfBoundsSet = "[set()]: Index out of bounds."
    export const indexOutOfBoundsSlice = "[slice()]: Index out of bounds."
    export const indexOutOfBoundsSwap = "[swap()]: Index out of bounds."
    export const invalidChunkSize = "[chunk()]: Chunk size must be at least one."
    export const invalidRandomBounds = "[random()]: Upper bound must be greater than or equal to the lower bound."
    export const invalidRandomBoundsZero = "[random()]: Upper bound must be greater than or equal to zero."
    export const invalidRangeBounds = "[range()]: The end bound must be greater than or equal to the start."
    export const invalidRangeStep = "[range()]: The step size cannot be zero."
    export const invalidSliceBounds = "[slice()]: Invalid slice bounds."
    export const lastElementNotFound = "[findLast()]: Element was not found."
    export const noFirstElement = "[first()]: No elements to take one element from."
    export const noLastElement = "[last()]: No elements to take last element from."
    export const noMaxElement = "[max()]: Cannot get maximum element from empty sequence."
    export const noMinElement = "[min()]: Cannot get minimum element from empty sequence."
    export const unpackAssertion = "[unpack()]: Unpacked value that did not exist."
    export const defaultPanic =
        "\n  " + "-".repeat(56) +
        "\n"   + " ".repeat(25) + "  PANIC  " +
        "\n  " + "-".repeat(56) +
        "\n"
}

/**
 * Container for LoDash functions.
 * @hidden
 */
export const LoDash = {
    clone: require("lodash.clone"),
    cloneDeep: require("lodash.clonedeep"),
    isEqual: require("lodash.isequal"),
    round: require("lodash.round")
}