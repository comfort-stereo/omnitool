export * from "./functions/static/copy"
export * from "./functions/static/equality"
export * from "./functions/static/error"
export * from "./functions/static/existence"
export * from "./functions/static/function"
export * from "./functions/static/math"
export * from "./functions/static/promise"
export * from "./functions/static/random"
export * from "./functions/static/sequence"
export * from "./functions/static/string"
export * from "./functions/static/time"
export * from "./functions/static/predicate"
export * from "./functions/extracted/omni-sequence"
