import {
    Block, Comparator, Dictionary, PairPredicate, Positional, Predicate, Sequence, SequenceIterator,
    Transform,
    Falsy
} from "../types"
import { identity } from "../../functions/static/function"
import { is } from "../../functions/static/equality"
import { OmniSequence, OutDestination, OutSource } from "./omni-sequence"
import { range } from "../../functions/static/sequence"
import { Span } from "./span"
import { ErrorMessage, isGreaterThan, isLessThan } from "../../shared";

/**
 * An omni-sequence that is only guaranteed be iterable once and and does not support random access.
 */
export class Pipe<T> implements OmniSequence<T> {

    /**
     * The underlying iterable of the pipe.
     */
    private _sequence: Sequence<T>

    /**
     * Creates a new pipe over any sequence.
     */
    constructor(sequence: Sequence<T>) {
        this._sequence = sequence
    }

    [Symbol.iterator](): Iterator<T> {
        return this._sequence[Symbol.iterator]()
    }

    toString(): string {
        return "Pipe::{?}"
    }

    all(predicate: Predicate<T>): boolean {
        for (const element of this) {
            if (!predicate(element)) {
                return false
            }
        }
        return true
    }

    as<O extends T>(this: Pipe<T>): OmniSequence<O> {
        return this as any
    }

    concat(elements: Sequence<T>): OmniSequence<T> {
        return new Pipe(generateAppend(this, elements))
    }

    append(...elements: T[]): OmniSequence<T> {
        return new Pipe(generateAppend(this, elements))
    }

    associate<K>(transform: Transform<T, K>): OmniSequence<[K, T]> {
        return this.map((element) => [transform(element), element] as [K, T])
    }

    at(index: number): T {
        index = this._sanitizeIndex(index)

        if (index < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsAt)
        }

        let i = 0
        for (const element of this) {
            if (i++ === index) {
                return element
            }
        }
        throw new Error(ErrorMessage.indexOutOfBoundsAt)
    }

    cleave(index: number): [T[], T[]] {
        index = this._sanitizeIndex(index)

        if (index < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsCleave)
        }
        const before: T[] = []
        const after: T[] = []
        let i = 0
        for (const element of this) {
            if (i++ < index) {
                before.push(element)
            } else {
                after.push(element)
            }
        }
        if (i < index) {
            throw new Error(ErrorMessage.indexOutOfBoundsCleave)
        }
        return [before, after] as [T[], T[]]
    }

    count(): number
    count(predicate: Predicate<T>): number
    count(predicate?: Predicate<T>): number {
        let count = 0
        if (predicate === undefined) {
            for (const element of this) {
                count++
            }
            return count
        }

        for (const element of this) {
            if (predicate(element)) {
                count++
            }
        }
        return count
    }

    cut(index: number): OmniSequence<T>
    cut(index: number, count: number): OmniSequence<T>
    cut(index: number, count: number = 1): OmniSequence<T> {
        index = this._sanitizeIndex(index)

        if (index < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsCut)
        }
        return new Pipe(generateCut(this, index, count))
    }

    chunk(count: number): OmniSequence<T[]> {
        count = this._sanitizeCount(count)

        if (count < 1) {
            throw new Error(ErrorMessage.invalidChunkSize)
        }
        return new Pipe(generateChunk(this, count))
    }

    difference<K>(sequence: Sequence<T>): OmniSequence<T>
    difference<K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
    difference<K>(sequence: Sequence<T>, transform: Transform<T, K> = identity): OmniSequence<T> {
        return new Pipe(generateDifference(this, sequence, transform as Transform<T, K>))
    }

    done(): T[] {
        return Array.from(this)
    }

    drop(): OmniSequence<T>
    drop(count: number): OmniSequence<T>
    drop(predicate: Predicate<T>): OmniSequence<T>
    drop(argument?: number | Predicate<T>): OmniSequence<T> {
        if (argument === undefined) {
            argument = 1
        }
        return new Pipe(generateDrop(this, typeof argument === "number" ? this._sanitizeCount(argument) : argument))
    }

    dropLast(): OmniSequence<T>
    dropLast(count: number): OmniSequence<T>
    dropLast(predicate: Predicate<T>): OmniSequence<T>
    dropLast(argument?: number | Predicate<T>): OmniSequence<T> {
        return this.solid().dropLast(argument as any)
    }

    each(action: (element: T, index: number, end: Block) => void): OmniSequence<T> {
        let exited = false
        const exit = () => {
            exited = true
        }
        let i = 0
        for (const element of this) {
            action(element, i++, exit)
            if (exited) {
                break
            }
        }
        return this
    }

    endsWith(suffix: Positional<T>): boolean
    endsWith(suffix: Positional<T>, equals: PairPredicate<T>): boolean
    endsWith(suffix: Positional<T>, equals: PairPredicate<T> = is): boolean {
        return this.solid().endsWith(suffix, equals)
    }

    entries(): OmniSequence<[number, T]> {
        return new Pipe(generateEntries(this))
    }

    entriesReversed(): OmniSequence<[number, T]> {
        return this.solid().entriesReversed()
    }

    filter(): OmniSequence<T>
    filter(predicate: Predicate<T>): OmniSequence<T>
    filter(predicate: Predicate<T> = identity): OmniSequence<T> {
        return new Pipe(generateFilter(this, predicate))
    }

    find(predicate: Predicate<T>): T | null
    find<O>(predicate: Predicate<T>, instead: O): T | O
    find<O>(predicate: Predicate<T>, instead?: O): T | O | null {
        for (const element of this) {
            if (predicate(element)) {
                return element
            }
        }
        if (instead === undefined) {
            return null
        }
        return instead
    }

    findLast(predicate: Predicate<T>): T | null
    findLast<O>(predicate: Predicate<T>, instead: O): T | O
    findLast<O>(predicate: Predicate<T>, instead?: O): T | O | null {
        let last: T | undefined = undefined
        for (const element of this) {
            if (predicate(element)) {
                last = element
            }
        }
        if (last !== undefined) {
            return last
        }
        if (instead === undefined) {
            return null
        }
        return instead
    }

    first<O>(): T
    first<O>(instead: O): T | O
    first<O>(instead?: O): T | O {
        for (const element of this) {
            return element
        }
        if (instead === undefined) {
            throw new Error(ErrorMessage.noFirstElement)
        }
        return instead
    }

    flat<T>(this: Pipe<Sequence<T>>): OmniSequence<T> {
        return new Pipe(generateFlat(this))
    }

    fold<R>(initial: R, reducer: (accumulator: R, current: T) => R): R {
        let value = initial
        for (const element of this) {
            value = reducer(value, element)
        }
        return value
    }

    index(predicate: Predicate<T>): number {
        return this.indices(predicate).first(-1)
    }

    indices(): OmniSequence<number>
    indices(predicate: Predicate<T>): OmniSequence<number>
    indices(predicate?: Predicate<T>): OmniSequence<number> {
        return new Pipe(generateIndices(this, predicate))
    }

    indicesReversed(): OmniSequence<number>
    indicesReversed(predicate: Predicate<T>): OmniSequence<number>
    indicesReversed(predicate?: Predicate<T>): OmniSequence<number> {
        if (predicate === undefined) {
            return range(0, this.count(), -1)
        }
        return this.solid().indicesReversed(predicate)
    }

    inject(index: number, elements: Sequence<T>): OmniSequence<T> {
        index = this._sanitizeIndex(index)

        if (index < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsInject)
        }
        return new Pipe(generateInject(this, index, elements, ErrorMessage.indexOutOfBoundsInject))
    }

    intersect<K>(sequence: Sequence<T>): OmniSequence<T>
    intersect<K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
    intersect<K>(sequence: Sequence<T>, transform: Transform<T, K> = identity): OmniSequence<T> {
        return new Pipe(generateIntersect(this, sequence, transform))
    }

    insert(index: number, ...elements: T[]): OmniSequence<T> {
        index = this._sanitizeIndex(index)

        if (index < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsInsert)
        }
        return new Pipe(generateInject(this, index, elements, ErrorMessage.indexOutOfBoundsInsert))
    }

    invert<K, V>(this: Pipe<[K, V]>): OmniSequence<[V, K]> {
        return new Pipe(generateInvert(this))
    }

    join(): string
    join(separator: string): string
    join(separator = ""): string {
        return Array.from(this).join(separator)
    }

    last<O>(): T
    last<O>(instead: O): T | O
    last<O>(instead?: O): T | O {
        const iterator = this[Symbol.iterator]()
        let current = iterator.next()
        if (current.done) {
            if (instead === undefined) {
                throw new Error(ErrorMessage.noLastElement)
            }
            return instead
        }

        let last: T
        while (true) {
            if (current.done) {
                break
            }
            last = current.value
            current = iterator.next()
        }

        return last!!
    }

    lastIndex(): number
    lastIndex(predicate: Predicate<T>): number
    lastIndex(predicate?: Predicate<T>): number {
        if (predicate === undefined) {
            return this.count() - 1
        }
        return this.indices(predicate).last(-1)
    }

    map<OK, OV>(this: Pipe<T>, transform: Transform<T, [OK, OV]>): OmniSequence<[OK, OV]>
    map<O>(this: Pipe<T>, transform: Transform<T, O>): OmniSequence<O>
    map<O>(this: Pipe<T>, transform: Transform<T, O>): OmniSequence<O> {
        return new Pipe(generateMap(this, transform))
    }

    max(this: Pipe<number>): number
    max(transform: Transform<T, number>): T
    max(transform?: Transform<T, number>): T | number {
        return findExtreme(this, transform || identity, -Infinity, isGreaterThan, ErrorMessage.noMaxElement)
    }

    mean(this: Pipe<number>): number {
        let sum = 0
        let count = 0
        for (const value of this) {
            sum += value
            count++
        }
        if (count === 0) {
            return 0
        }
        return sum / count
    }

    min(this: Pipe<number>): number
    min(transform: Transform<T, number>): T
    min(transform?: Transform<T, number>): T | number {
        return findExtreme(this, transform || identity, Infinity, isLessThan, ErrorMessage.noMinElement)
    }

    none(): boolean
    none(predicate: Predicate<T>): boolean
    none(predicate?: Predicate<T>): boolean {
        return !this.some(predicate as Predicate<T>)
    }

    partition(predicate: Predicate<T>): [T[], T[]] {
        const positive: T[] = []
        const negative: T[] = []
        for (const element of this) {
            if (predicate(element)) {
                positive.push(element)
            } else {
                negative.push(element)
            }
        }
        return [positive, negative]
    }

    permute(): OmniSequence<T[]> {
        return this.solid().permute()
    }

    prepend(...elements: T[]): OmniSequence<T> {
        return new Pipe(generatePrepend(this, elements))
    }

    rank(transform: Transform<T, number>): OmniSequence<T> {
        return this.solid().rank(transform)
    }

    remove(predicate: Predicate<T>): OmniSequence<T>
    remove(predicate: Predicate<T>, count: number): OmniSequence<T>
    remove(predicate: Predicate<T>, count = Infinity): OmniSequence<T> {
        return new Pipe(generateRemove(this, predicate, Math.floor(count)))
    }

    removeLast(predicate: Predicate<T>, count: number): OmniSequence<T> {
        return this.solid().removeLast(predicate, count)
    }

    repeat(): OmniSequence<T>
    repeat(count: number): OmniSequence<T>
    repeat(count?: number): OmniSequence<T> {
        return this.solid().repeat(count as number)
    }

    replace<R>(predicate: Predicate<T>, replacement: R): OmniSequence<T | R>
    replace<R>(predicate: Predicate<T>, replacement: R, count: number): OmniSequence<T | R>
    replace<R>(predicate: Predicate<T>, replacement: R, count = Infinity): OmniSequence<T | R> {
        return new Pipe(generateReplace(this, predicate, replacement, this._sanitizeCount(count)))
    }

    replaceLast<R>(predicate: Predicate<T>, replacement: R, count: number): OmniSequence<T | R> {
        return this.solid().replaceLast(predicate, replacement, this._sanitizeCount(count))
    }

    reversed(): OmniSequence<T> {
        return this.solid().reversed()
    }

    set(index: number, element: T): OmniSequence<T> {
        index = this._sanitizeIndex(index)

        if (index < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsSet)
        }
        return new Pipe(generateSet(this, index, element))
    }

    slice(start: number, end: number): OmniSequence<T> {
        start = this._sanitizeIndex(start)
        end = this._sanitizeIndex(end)

        if (start < 0 || end < start) {
            throw new Error(ErrorMessage.invalidSliceBounds)
        }
        return new Pipe(generateSlice(this, start, end))
    }

    solid(): Span<T> {
        return this.yank()
    }

    some(): boolean
    some(predicate: Predicate<T>): boolean
    some(predicate?: Predicate<T>): boolean {
        if (predicate === undefined) {
            for (const element of this) {
                return true
            }
            return false
        }
        return this.index(predicate) >= 0
    }

    sort(this: Pipe<number>): OmniSequence<number>
    sort(this: Pipe<number>, descending: boolean): OmniSequence<number>
    sort(this: Pipe<string>): OmniSequence<string>
    sort(this: Pipe<string>, descending: boolean): OmniSequence<string>
    sort(comparator: Comparator<T>): OmniSequence<T>
    sort(this: Pipe<T | number | string>, argument?: Comparator<T> | boolean): OmniSequence<T | number | string> {
        return this.solid().sort(argument as any)
    }

    startsWith(prefix: Sequence<T>): boolean
    startsWith(prefix: Sequence<T>, equals: PairPredicate<T>): boolean
    startsWith(prefix: Sequence<T>, equals: PairPredicate<T> = is): boolean {
        const iterator = this[Symbol.iterator]()
        const prefixIterator = prefix[Symbol.iterator]()

        while (true) {
            const current = iterator.next()
            const prefixResult = prefixIterator.next()
            if (prefixResult.done) {
                break
            }
            if (current.done) {
                return false
            }
            if (!equals(current.value, prefixResult.value)) {
                return false
            }
        }

        return true
    }

    sum(this: Pipe<number>): number {
        let sum = 0
        for (const value of this) {
            sum += value
        }
        return sum
    }

    swap(first: number, second: number): OmniSequence<T> {
        first = this._sanitizeIndex(first)
        second = this._sanitizeIndex(second)

        if (first < 0 || second < 0) {
            throw new Error(ErrorMessage.indexOutOfBoundsSwap)
        }
        const iterator = first < second
            ? generateSwap(this, first, second)
            : generateSwap(this, second, first)
        return new Pipe(iterator)
    }

    take(): OmniSequence<T>
    take(count: number): OmniSequence<T>
    take(predicate: Predicate<T>): OmniSequence<T>
    take(argument?: number | Predicate<T>): OmniSequence<T> {
        if (typeof argument === "number") {
            argument = this._sanitizeCount(argument)
        }
        return new Pipe(generateTake(this, argument === undefined ? Infinity : argument))
    }

    takeLast(count: number): OmniSequence<T>
    takeLast(predicate: Predicate<T>): OmniSequence<T>
    takeLast(argument: number | Predicate<T>): OmniSequence<T> {
        return this.solid().takeLast(argument as any)
    }

    toArray(): T[]
    toArray(destination: T[]): T[]
    toArray(destination: T[], clear: boolean): T[]
    toArray(destination?: T[], clear: boolean = false): T[] {
        if (destination) {
            if (clear) {
                destination.length = 0
            }
            destination.push(...this)
            return destination
        }

        return this.done()
    }

    toMap<K, V>(this: Pipe<[K, V]>): Map<K, V>
    toMap<K, V>(this: Pipe<[K, V]>, destination: Map<K, V>): Map<K, V>
    toMap<K, V>(this: Pipe<[K, V]>, destination: Map<K, V>, clear: boolean): Map<K, V>
    toMap<K, V>(this: Pipe<[K, V]>, destination?: Map<K, V>, clear: boolean = false): Map<K, V> {
        destination = destination || new Map()
        if (clear) {
            destination.clear()
        }
        for (const [key, value] of this) {
            destination.set(key, value)
        }
        return destination
    }

    toObject<V>(this: Pipe<[string, V]>): Dictionary<V>
    toObject<V>(this: Pipe<[string, V]>, destination: Dictionary<V>): Dictionary<V>
    toObject<V>(this: Pipe<[string, V]>, destination: Dictionary<V>, clear: boolean): Dictionary<V>
    toObject<V>(this: Pipe<[string, V]>, destination?: Dictionary<V>, clear: boolean = false): Dictionary<V> {
        if (destination) {
            if (clear) {
                for (const key in destination) {
                    if (destination.hasOwnProperty(key)) {
                        delete destination[key]
                    }
                }
            }
        } else {
            destination = {}
        }

        for (const [key, value] of this) {
            destination[key] = value
        }
        return destination
    }

    toSet(): Set<T>
    toSet(destination: Set<T>): Set<T>
    toSet(destination: Set<T>, clear: boolean): Set<T>
    toSet(destination?: Set<T>, clear: boolean = false): Set<T> {
        destination = destination || new Set()
        if (clear) {
            destination.clear()
        }
        for (const element of this) {
            destination.add(element)
        }
        return destination
    }

    union<K>(sequence: Sequence<T>): OmniSequence<T>
    union<K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>
    union<K>(sequence: Sequence<T>, transform: Transform<T, K> = identity): OmniSequence<T> {
        return new Pipe(generateUnion(this, sequence, transform))
    }

    unique<K>(): OmniSequence<T>
    unique<K>(transform: Transform<T, K>): OmniSequence<T>
    unique<K>(transform: Transform<T, K> = identity): OmniSequence<T> {
        return new Pipe(generateUnique(this, transform))
    }

    use<R>(transform: Transform<OmniSequence<T>, R>): R {
        return transform(this)
    }

    yank(): Span<T> {
        return new Span(Array.from(this)) as Span<T>
    }

    zip<V>(elements: Sequence<V>): OmniSequence<[T, V]> {
        return new Pipe(generateZip(this, elements))
    }

    private _sanitizeIndex(index: number) {
        return Math.floor(index)
    }

    private _sanitizeCount(count: number) {
        return Math.max(Math.floor(count), 0)
    }

    private _seat(sequence: Sequence<T>) {
        this._sequence = sequence
    }
}

function* generateAppend<T>(pipe: Pipe<T>, elements: Sequence<T>): SequenceIterator<T> {
    yield* pipe
    yield* elements
}

function* generateRemove<T>(pipe: Pipe<T>, predicate: Predicate<T>, count: number): SequenceIterator<T> {
    let removed = 0
    for (const element of pipe) {
        if (removed >= count) {
            yield element
        } else if (predicate(element)) {
            removed++
        } else {
            yield element
        }
    }
}

function* generateChunk<T>(pipe: Pipe<T>, count: number): SequenceIterator<T[]> {
    let current: T[] = []
    for (const element of pipe) {
        current.push(element)
        if (current.length === count) {
            yield current 
            current = []
        }
    }
    if (current.length !== 0) {
        yield current
    }
}

function* generateCut<T>(pipe: Pipe<T>, index: number, count: number): SequenceIterator<T> {
    let i = 0
    const end = index + count
    for (const element of pipe) {
        if (i < index || i >= end) {
            yield element
        }
        i++
    }

    if (i < index || i === 0) {
        throw new Error(ErrorMessage.indexOutOfBoundsCut)
    }
}

function* generateDifference<T, K>(pipe: Pipe<T>, sequence: Sequence<T>, 
                                   transform: Transform<T, K>): SequenceIterator<T> {

    sequence = Array.isArray(sequence) ? sequence : Array.from(sequence)

    const keys = new Set<K>() 
    const other = new Set<K>()

    for (const element of sequence) {
        other.add(transform(element))
    }

    for (const element of pipe) {
        const key = transform(element)
        if (keys.has(key)) {
            continue
        }
        keys.add(key)
        if (other.has(key)) {
            continue
        }
        yield element
    }

    other.clear()

    for (const element of sequence) {
        const key = transform(element)
        if (other.has(key)) {
            continue
        }
        other.add(key)
        if (keys.has(key)) {
            continue
        }
        yield element
    }
}

function* generateDrop<T>(pipe: Pipe<T>, argument: number | Predicate<T>): SequenceIterator<T> {
    if (typeof argument === "function") {
        const predicate = argument
        let done = false
        for (const element of pipe) {
            if (done) {
                yield element
            } else if (!predicate(element)) {
                yield element
                done = true
            }
        }
    } else {
        const count = argument
        let dropped = 0
        for (const element of pipe) {
            if (dropped >= count) {
                yield element
            } else {
                dropped++
            }
        }
    }
}

function* generateEntries<T>(pipe: Pipe<T>,): SequenceIterator<[number, T]> {
    let i = 0
    for (const element of pipe) {
        yield [i++, element]
    }
}

function* generateFilter<T>(pipe: Pipe<T>, predicate: Predicate<T>): SequenceIterator<T> {
    for (const element of pipe) {
        if (predicate(element)) {
            yield element
        }
    }
}

function* generateFlat<T>(pipe: Pipe<Sequence<T>>): SequenceIterator<T> {
    for (const element of pipe) {
        yield* element
    }
}

function* generateIndices<T>(pipe: Pipe<T>, predicate?: Predicate<T>): SequenceIterator<number> {
    if (predicate === undefined) {
        let i = 0
        for (const element of pipe) {
            yield i++
        }
    } else {
        let i = 0
        for (const element of pipe) {
            if (predicate(element)) {
                yield i
            }
            i++
        }
    }
}

function* generateInject<T>(pipe: Pipe<T>, index: number, elements: Sequence<T>,
                            errorMessage: string): SequenceIterator<T> {
    let i = 0
    for (const element of pipe) {
        if (i++ === index) {
            yield* elements
        }
        yield element
    }
    if (i === index) {
        yield* elements
    } else if (i < index) {
        throw new Error(errorMessage)
    }
}

function* generateIntersect<T, K>(pipe: Pipe<T>, sequence: Sequence<T>, 
                                  transform: Transform<T, K>): SequenceIterator<T> {
    const keys = new Set<K>()
    const other = new Set<K>()

    for (const element of sequence) {
        other.add(transform(element))
    }

    for (const element of pipe) {
        const key = transform(element)
        if (keys.has(key)) {
            continue
        }
        keys.add(key)
        if (other.has(key)) {
            yield element
        }
    }
}

function* generateInvert<K, V>(pipe: Pipe<[K, V]>): SequenceIterator<[V, K]> {
    for (const [key, value] of pipe) {
        yield [value, key]
    }
}

function* generateMap<T, O>(pipe: Pipe<T>, transform: Transform<T, O>): SequenceIterator<O> {
    for (const element of pipe) {
        yield transform(element)
    }
}

function* generatePositions<T>(pipe: Pipe<T>, predicate: Predicate<T>): SequenceIterator<number> {
    let i = 0
    for (const element of pipe) {
        if (predicate(element)) {
            yield i
        }
        i++
    }
}

function* generatePrepend<T>(pipe: Pipe<T>, elements: Sequence<T>): SequenceIterator<T> {
    yield* elements
    yield* pipe
}

function* generateReplace<T, R>(pipe: Pipe<T>, predicate: Predicate<T>, replacement: R,
                                count: number): SequenceIterator<T | R> {
    let replaced = 0
    for (const element of pipe) {
        if (replaced >= count) {
            yield element
        } else if (predicate(element)) {
            yield replacement
            replaced++
        } else {
            yield element
        }
    }
}

function* generateSet<T>(pipe: Pipe<T>, index: number, element: T): SequenceIterator<T> {
    let i = 0
    let invalid = true
    for (const other of pipe) {
        if (i++ === index) {
            yield element
            invalid = false
        } else {
            yield other
        }
    }

    if (invalid) {
        throw new Error(ErrorMessage.indexOutOfBoundsSet)
    }
}

function* generateSlice<T>(pipe: Pipe<T>, start: number, end: number): SequenceIterator<T> {
    let i = 0
    for (const element of pipe) {
        if (i >= start && i < end) {
            yield element
        }
        i++
    }
    if (i < start) {
        throw new Error(ErrorMessage.invalidSliceBounds)
    }
}

function* generateSwap<T>(pipe: Pipe<T>, first: number, second: number): SequenceIterator<T> {
    const iterator = pipe[Symbol.iterator]()
    const elements: T[] = []

    for (let i = 0; i <= second; i++) {
        const current = iterator.next()
        if (current.done) {
            throw new Error(ErrorMessage.indexOutOfBoundsSwap)
        }
        elements.push(current.value)
    }

    const temporary = elements[first]
    elements[first] = elements[second]
    elements[second] = temporary

    for (let i = 0; i < elements.length; i++) {
        yield elements[i]
    }

    while (true) {
        const current = iterator.next()
        if (current.done) {
            break
        }
        yield current.value
    }
}

function* generateTake<T>(pipe: Pipe<T>, argument: number | Predicate<T>): SequenceIterator<T> {
    if (typeof argument === "function") {
        const predicate = argument
        for (const element of pipe) {
            if (predicate(element)) {
                yield element
            } else {
                break
            }
        }
    } else {
        const count = argument
        let taken = 0
        for (const element of pipe) {
            if (taken++ >= count) {
                break
            }
            yield element
        }
    }
}

function* generateUnion<T, K>(pipe: Pipe<T>, sequence: Sequence<T>, 
                              transform: Transform<T, K>): SequenceIterator<T> {
    const seen = new Set<K>()

    for (const element of pipe) {
        const key = transform(element)
        if (seen.has(key)) {
            continue
        }
        yield element
        seen.add(key)
    }

    for (const element of sequence) {
        const key = transform(element)
        if (seen.has(key)) {
            continue
        }
        yield element
        seen.add(key)
    }
}

function* generateUnique<T, K>(pipe: Pipe<T>, transform: Transform<T, K>): SequenceIterator<T> {
    const indices: number[] = []
    const seen = new Set<K>()

    for (const element of pipe) {
        const key = transform(element)
        if (seen.has(key)) {
            continue
        }
        yield element
        seen.add(key)
    }
}

function* generateZip<T, V>(pipe: Pipe<T>, values: Sequence<V>): SequenceIterator<[T, V]> {
    const iterator = values[Symbol.iterator]()
    for (const key of pipe) {
        const result = iterator.next()
        if (result.done) {
            break
        }
        yield [key, result.value]
    }
}

function findExtreme<T>(pipe: Pipe<T>, transform: Transform<T, number>, start: number,
                        comparator: PairPredicate<number>, errorMessage: string): T {
    let extreme = start
    let result: T
    let found = false

    for (const element of pipe) {
        const value = transform(element)
        if (comparator(value, extreme)) {
            extreme = value
            result = element
            found = true
        }
    }

    if (found) {
        return result!!
    }

    throw new Error(errorMessage)
}