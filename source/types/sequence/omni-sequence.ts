import { Block, Comparator, Dictionary, PairPredicate, Positional, Predicate, Sequence, Transform, Falsy } from "../types"

export interface OmniSequence<T> extends Sequence<T> {

    /**
     * Returns an iterator over all elements of the sequence.
     */
        [Symbol.iterator](): Iterator<T>

    /*
     * Returns a string representation of the sequence.
     */
    toString(): string

    /**
     * Returns true if all elements match a **predicate** or the sequence is empty.
     */
    all(predicate: Predicate<T>): boolean

    /**
     * Casts the elements of the sequence to another type mid-chain. Does no actual operation.
     */
    as<O extends T>(this: OmniSequence<T>): OmniSequence<O>

    /**
     * Yields all elements followed by the **elements** in another sequence.
     */
    concat(elements: Sequence<T>): OmniSequence<T>

    /**
     * Yields all elements in the sequence, then the provided **elements**.
     */
    append(...elements: T[]): OmniSequence<T>

    /**
     * Yields key-value pairs for each element by mapping every element to a key by a provided **transform**.
     */
    associate<K>(transform: Transform<T, K>): OmniSequence<[K, T]>

    /**
     * Returns the element at the **index** position in the sequence.
     */
    at(index: number): T

    /**
     * Splits the sequence into two arrays at an **index**. The first contains all elements before the **index** and the
     * second contains all elements at and after the **index**.
     */
    cleave(index: number): [T[], T[]]

    /**
     * Returns the number of elements.
     */
    count(): number

    /**
     * Yields all elements except the element at a specified **index**.
     */
    cut(index: number): OmniSequence<T>

    /**
     * Removes a maximum of **count** elements after and including the element at a **start** index.
     */
    cut(index: number, count: number): OmniSequence<T>

    /**
     * Returns the number of elements matching a **predicate**.
     */
    count(predicate: Predicate<T>): number

    /**
     * Yields the elements of the sequence divided into arrays of length **count**.
     */
    chunk(count: number): OmniSequence<T[]>

    /**
     * Yields all unique elements not present in the other **sequence** then all unique elements in the other 
     * **sequence** that are not present in the original sequence. Equality is determined by strict equality.
     */
    difference<K>(sequence: Sequence<T>): OmniSequence<T>

    /**
     * Yields all unique elements not present in the other **sequence** then all unique elements in the other 
     * **sequence** that are not present in the original sequence. Equality is determined by strict equality of the 
     * result of the **transform** function on an element. 
     */
    difference<K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>

    /**
     * Converts the sequence into an array and returns it.
     */
    done(): T[]

    /**
     * Removes a single element from the start of the sequence.
     */
    drop(count: number): OmniSequence<T>

    /**
     * Removes **count** elements from the start of the sequence.
     */
    drop(count: number): OmniSequence<T>

    /**
     * Removes elements from the start of the sequence while a **predicate** is true.
     */
    drop(predicate: Predicate<T>): OmniSequence<T>

    /**
     * Removes a single element from the end of the sequence.
     */
    dropLast(): OmniSequence<T>

    /**
     * Removes **count** elements from the end of the sequence.
     */
    dropLast(count: number): OmniSequence<T>

    /**
     * Removes elements from the end of the sequence while a **predicate** is true.
     */
    dropLast(predicate: Predicate<T>): OmniSequence<T>

    /**
     * Iterates the sequence, applying the **action** to each element. The **action** is passed the current element,
     * the current index and an **end** callback. If **end** is called the loop will exit. Returns the same sequence.
     */
    each(action: (element: T, index: number, end: Block) => void): OmniSequence<T>

    /**
     * Returns true if the last elements of the sequence are equal to those of a **suffix** sequence by strict
     * equality.
     */
    endsWith(suffix: Positional<T>): boolean

    /**
     * Returns true if the last elements of the sequence are equal to those of a **suffix** sequence by a given
     * **equals** function.
     */
    endsWith(suffix: Positional<T>, equals: PairPredicate<T>): boolean

    /**
     * Yields index-value pairs for all elements.
     */
    entries(): OmniSequence<[number, T]>

    /**
     * Yields index-value pairs for all elements in reverse order.
     */
    entriesReversed(): OmniSequence<[number, T]>

    /**
     * Yields all elements that are considered truthy in a boolean context.
     */
    filter(): OmniSequence<T>

    /**
     * Yields all elements that match a **predicate**.
     */
    filter(predicate: Predicate<T>): OmniSequence<T>

    /**
     * Returns the first element that matches a **predicate**. Returns null if the element is not found.
     */
    find(predicate: Predicate<T>): T | null

    /**
     * Returns the first element that matches a **predicate**.  Returns **instead** if the element is not found.
     */
    find<O>(predicate: Predicate<T>, instead: O): T | O

    /**
     * Returns the last element that matches a **predicate**. Returns null if the element is not found.
     */
    findLast(predicate: Predicate<T>): T | null

    /**
     * Returns the last element that matches a **predicate**.  Returns **instead** if the element is not found.
     */
    findLast<O>(predicate: Predicate<T>, instead: O): T | O

    /**
     * Returns the first element. Throws an error if the sequence is empty.
     */
    first<O>(): T

    /**
     * Returns the first element. Returns **instead** if the sequence is empty.
     */
    first<O>(instead: O): T | O

    /**
     * Yields the elements of all sequences contained in a parent sequence.
     */
    flat<T>(this: OmniSequence<Sequence<T>>): OmniSequence<T>

    /**
     * Reduces all elements to a single value by continuously applying a **reducer** function to the accumulated value
     * and the current element. Requires an **initial** value to be provided as the starting accumulator value.
     */
    fold<R>(initial: R, reducer: (accumulator: R, current: T) => R): R

    /**
     * Returns the index of the first element that matches a **predicate**.
     */
    index(predicate: Predicate<T>): number

    /**
     * Yields the indices of all elements.
     */
    indices(): OmniSequence<number>

    /**
     * Yields the indices of all elements matching a **predicate**.
     */
    indices(predicate: Predicate<T>): OmniSequence<number>

    /**
     * Yields the indices of all elements in reverse order.
     */
    indicesReversed(): OmniSequence<number>

    /**
     * Yields the indices of all elements matching a **predicate** in reversed order.
     */
    indicesReversed(predicate: Predicate<T>): OmniSequence<number>

    /**
     * Yields all elements with the **elements** of another sequence inserted at a specified **index**.
     */
    inject(index: number, elements: Sequence<T>): OmniSequence<T>
    
    /**
     * Yields all unique elements that are also present in another **sequence**. Equality is
     * determined by strict equality.
     */
    intersect<K>(sequence: Sequence<T>): OmniSequence<T>

    /**
     * Yields all unique elements that are also present in another **sequence**. Equality is determined by strict 
     * equality of the result of the **transform** function applied to an element.
     */
    intersect<K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>

    /**
     * Yields all elements with the specified elements inserted at an **index**.
     */
    insert(index: number, ...elements: T[]): OmniSequence<T>

    /**
     * Flips the sequence of key-value pairs such that the key becomes the value and the value becomes the key.
     */
    invert<K, V>(this: OmniSequence<[K, V]>): OmniSequence<[V, K]>

    /**
     * Joins all elements as strings with no separator.
     */
    join(): string

    /**
     * Joins all elements as strings using a **separator**.
     */
    join(separator: string): string

    /**
     * Returns the last element. Throws an error if the sequence is empty.
     */
    last<O>(): T

    /**
     * Returns the last element. Returns **instead** if the sequence is empty.
     */
    last<O>(instead: O): T | O

    /**
     * Returns the index of the last element. Returns negative one if the sequence is empty.
     */
    lastIndex(): number

    /**
     * Returns the index of the last element that matches a predicate. Returns negative one if the element is not
     * found.
     */
    lastIndex(predicate: Predicate<T>): number

    /**
     * Applies a **transform** to all elements, yielding the results as key-value pairs.
     */
    map<OK, OV>(this: OmniSequence<T>, transform: Transform<T, [OK, OV]>): OmniSequence<[OK, OV]>

    /**
     * Applies a **transform** to all elements, yielding the results.
     */
    map<O>(this: OmniSequence<T>, transform: Transform<T, O>): OmniSequence<O>

    /**
     * Returns the maximum number from the sequence of numbers.
     */
    max(this: OmniSequence<number>): number

    /**
     * Returns the element for which a **transform** returns the largest number.
     */
    max(transform: Transform<T, number>): T

    /**
     * Returns the average of the sequence of numbers.
     */
    mean(this: OmniSequence<number>): number

    /**
     * Returns the minimum number from the sequence of numbers.
     */
    min(this: OmniSequence<number>): number

    /**
     * Returns the element for which a **transform** returns the smallest number.
     */
    min(transform: Transform<T, number>): T

    /**
     * Returns true if the sequence is empty.
     */
    none(): boolean

    /**
     * Returns true if no elements match a **predicate** or the sequence is empty.
     */
    none(predicate: Predicate<T>): boolean

    /**
     * Divides all elements into two arrays by a **predicate**. The first contains the elements that match the
     * **predicate** and the second contains the elements that don't.
     */
    partition(predicate: Predicate<T>): [T[], T[]]

    /** 
     * Yields all permutations of the sequence as arrays.
     */
    permute<T>(): OmniSequence<T[]>

    /**
     * Yields the provided **elements** then all elements in the sequence.
     */
    prepend(...elements: T[]): OmniSequence<T>

    /**
     * Yields all elements sorted in ascending order by their value according to a **transform**.
     */
    rank(transform: Transform<T, number>): OmniSequence<T>

    /**
     * Removes all elements that match a **predicate**.
     */
    remove(predicate: Predicate<T>): OmniSequence<T>

    /**
     * Remove **count** elements that match a predicate.
     */
    remove(predicate: Predicate<T>, count: number): OmniSequence<T>

    /**
     * Remove **count** elements that match a **predicate** from the end of the sequence.
     */
    removeLast(predicate: Predicate<T>, count: number): OmniSequence<T>

    /**
     * Repeats the sequence infinitely, returning to the first element after the last is yielded.
     */
    repeat(): OmniSequence<T>

    /**
     * Repeats the sequence **count** times, returning to the first element after the last is yielded.
     */
    repeat(count: number): OmniSequence<T>

    /**
     * Replaces all elements matching a **predicate** with a **replacement** value.
     */
    replace<R>(predicate: Predicate<T>, replacement: R): OmniSequence<T | R>

    /**
     * Replaces **count** elements matching a **predicate** with a **replacement** value.
     */
    replace<R>(predicate: Predicate<T>, replacement: R, count: number): OmniSequence<T | R>

    /**
     * Replaces the last **count** elements matching a **predicate** with a **replacement** value.
     */
    replaceLast<R>(predicate: Predicate<T>, replacement: R, count: number): OmniSequence<T | R>

    /**
     * Yields all elements in reverse order.
     */
    reversed(): OmniSequence<T>

    /**
     * Yields all elements with the element at an **index** replaced with another **element**.
     */
    set(index: number, element: T): OmniSequence<T>

    /**
     * Yields all elements after and including the **start** index but before the **end** index.
     */
    slice(start: number, end: number): OmniSequence<T>

    /**
     * Returns true if the sequence is not empty.
     */
    some(): boolean

    /**
     * Returns true if any element matches a **predicate**.
     */
    some(predicate: Predicate<T>): boolean

    /**
     * Yields the sequence of numbers sorted in ascending order.
     */
    sort(this: OmniSequence<number>): OmniSequence<number>

    /**
     * Yields the sequence of numbers sorted in ascending or **descending** order.
     */
    sort(this: OmniSequence<number>, descending: boolean): OmniSequence<number>

    /**
     * Yields the sequence of numbers sorted in ascending lexicographic order.
     */
    sort(this: OmniSequence<string>): OmniSequence<string>

    /**
     * Yields the sequence of numbers sorted in ascending or **descending** lexicographic order.
     */
    sort(this: OmniSequence<string>, descending: boolean): OmniSequence<string>

    /**
     * Yields the sequence of numbers sorted by a **comparator** function.
     */
    sort(comparator: Comparator<T>): OmniSequence<T>

    /**
     * Returns true if the first elements of the sequence are equal to those of a **suffix** sequence by strict
     * equality.
     */
    startsWith(prefix: Sequence<T>): boolean

    /**
     * Returns true if the first elements of the sequence are equal to those of a **suffix** sequence by a given
     * **equals** function.
     */
    startsWith(prefix: Sequence<T>, equals: PairPredicate<T>): boolean

    /**
     * Returns the sum of the sequence of numbers.
     */
    sum(this: OmniSequence<number>): number

    /**
     * Yields all elements with the elements at the indices **first** and **second** swapped.
     */
    swap(first: number, second: number): OmniSequence<T>

    /**
     * Yields all elements.
     */
    take(): OmniSequence<T>

    /**
     * Yields the first **count** elements.
     */
    take(count: number): OmniSequence<T>

    /**
     * Yields elements from the start of the sequence while a **predicate** is true.
     */
    take(predicate: Predicate<T>): OmniSequence<T>

    /**
     * Yields the last **count** elements.
     */
    takeLast(count: number): OmniSequence<T>

    /**
     * Yields elements from the end of the sequence while a **predicate** is true. The elements are yielded in their
     * original order.
     */
    takeLast(predicate: Predicate<T>): OmniSequence<T>

    /**
     * Converts the sequence into an array and returns it.
     */
    toArray(): T[]

    /**
     * Appends the elements in the sequence to a **destination** array and returns the **destination**.
     */
    toArray(destination: T[]): T[]

    /**
     * Appends the elements in the sequence to a **destination** array and returns the **destination**.
     * Clears the **destination** before appending if **clear** is true.
     */
    toArray(destination: T[], clear: boolean): T[]

    /**
     * Converts the sequence of key-value pairs into a map and returns it.
     */
    toMap<K, V>(this: OmniSequence<[K, V]>): Map<K, V>

    /**
     * Outputs the sequence of key-value pairs to a **destination** map and returns the **destination**.
     */
    toMap<K, V>(this: OmniSequence<[K, V]>, destination: Map<K, V>): Map<K, V>

    /**
     * Outputs the sequence of key-value pairs to a **destination** map and returns the **destination**.
     * Clears the **destination** before outputting if **clear** is true.
     */
    toMap<K, V>(this: OmniSequence<[K, V]>, destination: Map<K, V>, clear: boolean): Map<K, V>

    /**
     * Converts the sequence of string-value pairs into an object and returns it.
     */
    toObject<V>(this: OmniSequence<[string, V]>): Dictionary<V>

    /**
     * Outputs the sequence of string-value to a **destination** object and returns the **destination**.
     */
    toObject<V>(this: OmniSequence<[string, V]>, destination: Dictionary<V>): Dictionary<T>

    /**
     * Outputs the sequence of string-value pairs **destination** object and returns the **destination**. 
     * Clears the **destination** before outputting if **clear** is true.
     */
    toObject<V>(this: OmniSequence<[string, V]>, destination: Dictionary<V>, clear: boolean): Dictionary<V>

    /**
     * Converts the sequence into a set and returns it.
     */
    toSet(): Set<T>

    /**
     * Adds the elements in the sequence to a **destination** set and returns the **destination**.
     */
    toSet(destination: Set<T>): Set<T>

    /**
     * Adds the elements in the sequence to a **destination** set and returns the **destination**.
     * Clears the **destination** before adding if **clear** is true.
     */
    toSet(destination: Set<T>, clear: boolean): Set<T>

    /**
     * Yields all unique elements, then all unique elements from another **sequence** if they have not 
     * already been yielded. Equality is determined by strict equality.
     */
    union<K>(sequence: Sequence<T>): OmniSequence<T>

    /**
     * Yields all unique elements, then all unique elements from another **sequence** if they have not 
     * already been yielded. Equality is determined by strict equality of the result of the **transform** function 
     * applied to an element.
     */
    union<K>(sequence: Sequence<T>, transform: Transform<T, K>): OmniSequence<T>

    /**
     * Yields elements unique to all other elements by strict equality. Only the first occurrence of each element is
     * kept.
     */
    unique<K>(): OmniSequence<T>

    /**
     * Yields elements unique to all other elements according to the value returned by a **transform** function. The
     * values returned by the transform function are compared using strict equality. Only the first occurrence of
     * each element is kept.
     */
    unique<K>(transform: Transform<T, K>): OmniSequence<T>

    /**
     * Applies a **transform** to the sequence and returns the result.
     */
    use<R>(transform: Transform<OmniSequence<T>, R>): R

    /**
     * Returns a version of the sequence that is iterable multiple times and supports random access. If the sequence
     * already matches these conditions the same sequence will be returned.
     */
    solid(): OmniSequence<T>

    /**
     * The same as [[OmniSequence.solid]] but guaranteed create a new underlying array. The returned sequence
     * will be completely independent of the original source iterable.
     */
    yank(): OmniSequence<T>

    /**
     * Combines the elements of the sequence with the **elements** of another sequence, yielding key-value pairs with
     * keys from the original sequence and values from the other sequence. Yields a number of pairs equal to the
     * length of the shortest sequence.
     */
    zip<V>(elements: Sequence<V>): OmniSequence<[T, V]>
}

/**
 * @hidden
 */
export type OutSource<K, T> = OmniSequence<T> | OmniSequence<[K, T]> | OmniSequence<[string, T]>

/**
 * @hidden
 */
export type OutDestination<K, T> = T[] | Set<T> | Dictionary<T> | Map<K, T>
