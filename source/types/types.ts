/**
 * A function that performs an operation on an input **value** but returns nothing.
 */
export type Action<T> = (value: T) => void

/**
 * A function with no parameters that returns nothing.
 */
export type Block = () => void

/**
 * A function used in sorting algorithms to determine the sort order.
 */
export type Comparator<T> = (value: T, other: T) => number

/**
 * A function that returns a truthy or falsy value given two input values. Commonly used as an equality predicate.
 */
export type PairPredicate<T> = (value: T, other: T) => any

/**
 * A function used to match or not match a given input value by returning a truthy of falsy value.
 */
export type Predicate<T> = (value: T) => any

/**
 * A function with no parameters that returns a single value.
 */
export type Producer<T> = () => T

/**
 * A function that maps an input **value** to a return value.
 */
export type Transform<I, O> = (value: I) => O

/**
 * An object with values of a given type.
 */
export type Dictionary<T> = { [key: string]: T }

/**
 * A type with random access. Includes arrays and strings.
 */
export type Positional<T> = ArrayLike<T> & Iterable<T> & (T[] | string)

/**
 * An iterable object.
 */
export type Sequence<T> = Iterable<T>

/**
 * A generator-type object.
 */
export type SequenceIterator<T> = IterableIterator<T>

/**
 * A value that is considered falsy in a boolean context. 
 */
export type Falsy = undefined | null | false | 0 | ""

/**
 * A primitive object.
 */
export type Primitive = undefined | null | boolean | number | string | Symbol