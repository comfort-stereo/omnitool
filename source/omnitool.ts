import { injectPipeConstructor, Span } from "./types/sequence/span"
import { Pipe } from "./types/sequence/pipe"
import * as Omnitool from "./library"

export { Pipe } from "./types/sequence/pipe"
export { Span } from "./types/sequence/span"

export * from "./library"

injectPipeConstructor(Pipe)

/**
 * The omnitool object. Provides access to all library functions.
 */
export const O = Omnitool
