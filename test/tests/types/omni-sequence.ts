import { Pipe } from "../../../source/types/sequence/pipe"
import { Span } from "../../../source/types/sequence/span"
import { expectElements } from "../../utilities"
import { O } from "../../../source/omnitool";

describe("pipe", () => {

    describe("constructor", () => {
        const numbers = [1, 2, 3]
        test("yields all elements in an iterable", () => {
            expectElements(new Pipe(numbers), numbers)
        })
    })

    describe("is iterable", () => {
        const numbers = [1, 2, 3]
        expect(new Pipe(numbers)[Symbol.iterator]).toBeDefined()
        const elements: number[] = []
        for (const element of new Pipe(numbers)) {
            elements.push(element)
        }
        expect(elements).toEqual(numbers)
    })

    describe("can be expanded in an array literal", () => {
        expect([0, ...new Pipe([1, 2, 3])]).toEqual([0, 1, 2, 3])
    })

    describe("as", () => {
        new Pipe([1, 2, null, 3, 4]).filter().as<number>().each((element) => O.noop(element + 1))
    })
})

describe("span", () => {

    test("constructor", () => {
        const numbers = [1, 2, 3, 4, 5]
        expect(new Span(numbers).toArray()).toEqual(numbers)
        expect(new Span(numbers, 2).toArray()).toEqual([3, 4, 5])
        expect(new Span(numbers, 10).toArray()).toEqual([])
        expect(new Span(numbers, 0, 2).toArray()).toEqual([1, 2])
        expect(new Span(numbers, 2, 4).toArray()).toEqual([3, 4])
    })

    describe("is iterable", () => {
        const numbers = [1, 2, 3]
        expect(new Span(numbers)[Symbol.iterator]).toBeDefined()
        const elements: number[] = []
        for (const element of new Span(numbers)) {
            elements.push(element)
        }
        expect(elements).toEqual(numbers)
    })

    describe("can be expanded in an array literal", () => {
        expect([0, ...new Span([1, 2, 3])]).toEqual([0, 1, 2, 3])
    })

    describe("as", () => {
        new Span([1, 2, null, 3, 4]).filter().as<number>().each((element) => O.noop(element + 1))
    })
})