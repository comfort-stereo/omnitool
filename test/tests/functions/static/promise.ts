import { O } from "../../../../source/omnitool"

describe("promise", () => {
    describe("delay", () => {
        describe("no argument", () => {
            test("resolves on next tick", () => {
                const start = Date.now()
                const first = O.delay()
                O.delay().then(() => {
                    expect(Date.now() - start).toBeLessThanOrEqual(2)
                })
            })
        })

        describe("with delay", () => {
            test("resolves after given delay in ms", () => {
                const start = Date.now()
                O.delay(50).then(() => {
                    expect(Date.now() - start).toBeCloseTo(50)
                })
            })

            test("resolves immediately if the delay is zero", () => {
                const start = Date.now()
                O.delay(0).then(() => {
                    expect(Date.now() - start).toBeLessThanOrEqual(2)
                })
            })

            test("resolves immediately if the delay is less than zero", () => {
                const start = Date.now()
                O.delay(-250).then(() => {
                    expect(Date.now() - start).toBeLessThanOrEqual(2)
                })
            })
        })
    })
})