import { O } from "../../../../source/omnitool"
import { errorTag } from "../../../utilities"

describe("random functions", () => {
    describe("chance", () => {
        const repetitions = 5000

        test("returns true at a frequency specified by a probability", () => {
            let positive = 0
            let negative = 0
            O.times(repetitions, () => {
                if (O.chance(0.75)) {
                    positive++
                } else {
                    negative++
                }
            })
            expect(positive).toBeCloseTo(repetitions * 0.75, -3)
            expect(negative).toBeCloseTo(repetitions * 0.25, -3)
        })

        test("always returns true if the probability is one", () => {
            O.times(repetitions, () => {
                expect(O.chance(1)).toEqual(true)
            })
        })

        test("always returns true if the probability greater than one", () => {
            O.times(repetitions, () => {
                expect(O.chance(2)).toEqual(true)
            })
        })

        test("always returns false if the probability is zero", () => {
            O.times(repetitions, () => {
                expect(O.chance(0)).toEqual(false)
            })
        })

        test("always returns false if the probability is negative", () => {
            O.times(repetitions, () => {
                expect(O.chance(-1)).toEqual(false)
            })
        })
    })

    describe("random", () => {
        const repetitions = 1000

        describe("no arguments", () => {
            test("generates a random float between zero and one", () => {
                O.times(repetitions, () => {
                    const value = O.random()
                    expect(value).toBeGreaterThanOrEqual(0)
                    expect(value).toBeLessThanOrEqual(1)
                })
            })
        })

        describe("upper bound", () => {
            test("generates a random float between zero and the provided value", () => {
                O.times(repetitions, () => {
                    const value = O.random(100)
                    expect(value).toBeGreaterThanOrEqual(0)
                    expect(value).toBeLessThanOrEqual(100)
                })
            })

            test("throws an error if the provided value is less than zero", () => {
                expect(() => O.random(-10)).toThrowError(errorTag("random"))
            })
        })

        describe("lower and upper bound", () => {
            test("generates a random float between the lower bound and the upper bound", () => {
                O.times(repetitions, () => {
                    const value = O.random(-5, 5)
                    expect(value).toBeGreaterThanOrEqual(-5)
                    expect(value).toBeLessThanOrEqual(5)
                })
            })

            test("throws an error if the provided value is less than zero", () => {
                expect(() => O.random(5, -5)).toThrowError(errorTag("random"))
            })
        })
    })

    describe("randomInteger", () => {
        const repetitions = 1000

        describe("upper bound", () => {
            test("generates a random float between zero and the provided value", () => {
                O.times(repetitions, () => {
                    const value = O.randomInteger(100)
                    expect(value).toBeGreaterThanOrEqual(0)
                    expect(value).toBeLessThanOrEqual(100)
                    expect(Math.round(value)).toEqual(value)
                })
            })

            test("throws an error if the provided value is less than zero", () => {
                expect(() => O.randomInteger(-10)).toThrowError(errorTag("random"))
            })
        })

        describe("lower and upper bound", () => {
            test("generates a random float between the lower bound and the upper bound", () => {
                O.times(repetitions, () => {
                    const value = O.randomInteger(-5, 5)
                    expect(value).toBeGreaterThanOrEqual(-5)
                    expect(value).toBeLessThanOrEqual(5)
                    expect(Math.round(value)).toEqual(value)
                })
            })

            test("throws an error if the provided value is less than zero", () => {
                expect(() => O.randomInteger(5, -5)).toThrowError(errorTag("random"))
            })
        })
    })
})