import { O } from "../../../../source/omnitool"

describe("copy functions", () => {
    describe("clone", () => {
        test("deep copies a value and its contents", () => {
            const object = {
                array: [1, 2, 3]
            }

            const cloned = O.clone(object)
            expect(cloned).toEqual(object)
            expect(object === cloned).toEqual(false)
            expect(object.array).toEqual(cloned.array)
            expect(object.array === cloned.array).toEqual(false)
        })
    })

    describe("copy", () => {
        test("shallow copies a value", () => {
            const object = {
                string: "hello",
                array: [1, 2, 3]
            }

            const copied = O.copy(object)
            expect(copied).toEqual(object)
            expect(object === copied).toEqual(false)
            expect(object.array).toEqual(copied.array)
            expect(object.array === copied.array).toEqual(true)
        })
    })
})