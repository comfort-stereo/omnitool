import { errorTag, expectElements } from "../../../utilities"
import { O } from "../../../../source/omnitool"

describe("sequence functions", () => {
    describe("keys", () => {
        const object = {hawk: 1, cougar: 2, bear: 3}
        const prototype = {ignored: 5}
        Object.setPrototypeOf(object, prototype)

        test("yields all own-property names in an object", () => {
            expectElements(O.keys(object), ["hawk", "cougar", "bear"])
        })

        test("handles empty", () => {
            expectElements(O.keys({}), [])
        })
    })

    describe("pairs", () => {
        const object = {hawk: 1, cougar: 2, bear: 3}
        const prototype = {ignored: 5}
        Object.setPrototypeOf(object, prototype)

        test("yields all own-property-value pairs in an object", () => {
            expectElements(O.pairs(object), [["hawk", 1], ["cougar", 2], ["bear", 3]])
        })

        test("handles empty", () => {
            expectElements(O.pairs({}), [])
        })
    })

    describe("range", () => {
        describe("one argument", () => {
            test("yields numbers between zero and the provided integer with an increment of one", () => {
                expectElements(O.range(5), [0, 1, 2, 3, 4])
            })

            test("handles zero", () => {
                expectElements(O.range(0), [])
            })

            test("throws an error if the argument is negative", () => {
                expect(() => O.range(-10)).toThrowError(errorTag("range"))
            })
        })

        describe("two arguments", () => {
            test("yields numbers between the first number and the second number (non-inclusive) with an increment" +
                " of one", () => {
                expectElements(O.range(0, 5), [0, 1, 2, 3, 4])
                expectElements(O.range(5, 10), [5, 6, 7, 8, 9])
                expectElements(O.range(5.5, 10), [5.5, 6.5, 7.5, 8.5, 9.5])
                expectElements(O.range(-3, 3), [-3, -2, -1, 0, 1, 2])
            })

            test("handles empty", () => {
                expectElements(O.range(0, 0), [])
                expectElements(O.range(5, 5), [])
                expectElements(O.range(5.5, 5.5), [])
                expectElements(O.range(-5, -5), [])
            })

            test("throws an error if the end is less than the start", () => {
                expect(() => O.range(5, -5)).toThrowError(errorTag("range"))
                expect(() => O.range(0, -5)).toThrowError(errorTag("range"))
            })
        })

        describe("with step", () => {
            test("yields numbers in step sized intervals", () => {
                expectElements(O.range(0, 5, 2), [0, 2, 4])
                expectElements(O.range(5, 10, 2), [5, 7, 9])
                expectElements(O.range(5.5, 10.5, 2), [5.5, 7.5, 9.5])
            })

            test("the step can be a float", () => {
                expectElements(O.range(0, 3, 0.5), [0, 0.5, 1, 1.5, 2, 2.5])
                expectElements(O.range(5, 7, 0.5), [5, 5.5, 6, 6.5])
                expectElements(O.range(5.5, 7.5, 0.5), [5.5, 6, 6.5, 7])
            })

            test("if the step is negative the range will be reversed", () => {
                expectElements(O.range(0, 5, -1), [4, 3, 2, 1, 0])
                expectElements(O.range(5, 10, -1), [9, 8, 7, 6, 5])
                expectElements(O.range(5, 7, -0.5), [6.5, 6, 5.5, 5])
                expectElements(O.range(5.5, 7.5, -0.5), [7, 6.5, 6, 5.5])
                expectElements(O.range(0, 0, -1), [])
            })

            test("throws an error if the step is zero", () => {
                expect(() => O.range(5, 10, 0)).toThrowError(errorTag("range"))
            })
        })
    })

    describe("sequence", () => {
        describe("no initial value", () => {
            test("yields an infinite sequence where each element is the result of a callback function", () => {
                expectElements(O.sequence(() => 0).take(5), [0, 0, 0, 0, 0])
                expectElements(O.sequence(() => [0, 0]).take(3), [[0, 0], [0, 0], [0, 0]])
            })
        })

        describe("initial value", () => {
            test("yields an infinite sequence where each element is generated by passing the previous element into the" +
                " callback", () => {
                expectElements(O.sequence(1, (current) => current * 2).take(5), [1, 2, 4, 8, 16])
            })
        })
    })

    describe("values", () => {
        const object = {hawk: 1, cougar: 2, bear: 3}
        const prototype = {ignored: 5}
        Object.setPrototypeOf(object, prototype)

        test("yields all own-property values in an object", () => {
            expectElements(O.values(object), [1, 2, 3])
        })

        test("handles empty", () => {
            expectElements(O.values({}), [])
        })
    })
})