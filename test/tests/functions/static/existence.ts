import { O } from "../../../../source/omnitool"
import { errorTag } from "../../../utilities"

describe("existence functions", () => {
    describe("unpack", () => {
        const undefinedNumber: number | undefined = undefined
        const nullNumber: number | null = null

        test("returns the input value if it is not null or undefined", () => {
            expect(O.unpack(1)).toEqual(1)
            expect(O.unpack([1])).toEqual([1])
        })

        test("returns a default value if the input value is null or undefined and a default is provided", () => {
            expect(O.unpack(undefinedNumber, 10)).toEqual(10)
            expect(O.unpack(nullNumber, 10)).toEqual(10)
            expect(O.unpack(undefinedNumber, null)).toEqual(null)
            expect(O.unpack(nullNumber, null)).toEqual(null)
        })

        test("throws an error if the input value is null or undefined and no default is provided", () => {
            expect(() => O.unpack(undefinedNumber)).toThrowError(errorTag("unpack"))
            expect(() => O.unpack(nullNumber)).toThrowError(errorTag("unpack"))
        })

        test("type guard", () => {
            const value: number | undefined = 0
            const result = O.unpack(value)
            O.noop(result + 1)
        })
    })
})