import { O } from "../../../../source/omnitool"

describe("error functions", () => {
    describe("panic", () => {
        test("throws a an error", () => {
            expect(() => O.panic()).toThrowError()
        })

        test("throws an error with an error message if provided", () => {
            expect(() => O.panic("ERROR")).toThrowError("ERROR")
            expect(() => O.panic("")).toThrowError("")
        })
    })

    describe("snag", () => {
        test("returns the thrown error if a callback throws an error", () => {
            expect(O.snag(() => {
                throw new Error("TEST")
            })).toEqual(new Error("TEST"))
        })

        test("returns null otherwise", () => {
            expect(O.snag(() => "TEST")).toEqual(null)
        })
    })
})