import { O } from "../../../../source/omnitool"

describe("equality functions", () => {
    describe("equal", () => {
        const object = {
            string: "hello",
            array: [1, 2, 3]
        }

        describe("two arguments", () => {
            test("returns true if two values are structurally equivalent", () => {
                expect(O.equal(object, {string: "hello", array: [1, 2, 3]})).toEqual(true)
            })

            test("returns false otherwise", () => {
                expect(O.equal(object, {string: "goodbye", array: [1, 2, 3]})).toEqual(false)
            })
        })

        describe("one argument", () => {
            test("returns a predicate that returns true if a value is structurally equivalent to the original value",
                () => {
                    expect(O.equal(object)({string: "hello", array: [1, 2, 3]})).toEqual(true)
                })

            test("returns false otherwise", () => {
                expect(O.equal(object)({string: "goodbye", array: [1, 2, 3]})).toEqual(false)
            })
        })
    })

    describe("is", () => {
        describe("two arguments", () => {
            test("returns the result of a strict equality between a value and another value", () => {
                expect(O.is(1, 1)).toEqual(true)
                expect(O.is(1, 0)).toEqual(false)
                expect(O.is({}, {})).toEqual(false)
            })
        })

        describe("single argument", () => {
            test("returns a function that returns the result of a strict equality between the original value and" +
                " another value", () => {
                expect(O.is(1)(1)).toEqual(true)
                expect(O.is(1)(0)).toEqual(false)
                expect(O.is({})({})).toEqual(false)
            })
        })
    })

    describe("isnt", () => {
        describe("two arguments", () => {
            test("returns the result of a strict inequality between a value and another value", () => {
                expect(O.isnt(1, 1)).toEqual(false)
                expect(O.isnt(1, 0)).toEqual(true)
                expect(O.isnt({}, {})).toEqual(true)
            })
        })

        describe("single argument", () => {
            test("returns a function that returns the result of a strict inequality between the original value and" +
                " another value", () => {
                expect(O.isnt(1)(1)).toEqual(false)
                expect(O.isnt(1)(0)).toEqual(true)
                expect(O.isnt({})({})).toEqual(true)
            })
        })
    })

    describe("unequal", () => {
        const object = {
            string: "hello",
            array: [1, 2, 3]
        }

        describe("two arguments", () => {
            test("returns true if two values are not structurally equivalent", () => {
                expect(O.unequal(object, {string: "hello", array: [1, 2, 3]})).toEqual(false)
            })

            test("returns false otherwise", () => {
                expect(O.unequal(object, {string: "goodbye", array: [1, 2, 3]})).toEqual(true)
            })
        })

        describe("one argument", () => {
            test("returns a predicate that returns true if a value is not structurally equivalent to the original" +
                " value", () => {
                    expect(O.unequal(object)({string: "hello", array: [1, 2, 3]})).toEqual(false)
                })

            test("returns false otherwise", () => {
                expect(O.unequal(object)({string: "goodbye", array: [1, 2, 3]})).toEqual(true)
            })
        })
    })
})
