import { O } from "../../../../source/omnitool"

describe("time functions", () => {
    describe("duration", () => {
        test("immediately invokes a callback and returns the time in ms it takes to run", () => {
            const delay = 150
            const fudge = 5
            const start = Date.now()
            const time = O.duration(() => {
                while (Date.now() - start < delay) {
                }
            })
            expect(time).toBeLessThan(delay + fudge)
            expect(time).toBeGreaterThan(delay - fudge)
        })
    })

    describe("now", () => {
        test("returns the number of milliseconds since the UNIX epoch", () => {
            O.times(100, () => {
                const beforeTime = Date.now()
                const time = O.now()
                const afterTime = Date.now()
                expect(time).toBeGreaterThanOrEqual(beforeTime)
                expect(time).toBeLessThanOrEqual(afterTime)
            })
        })
    })

    describe("sleep", () => {
        test("blocks the current thread for a given number of milliseconds", () => {
            const delay = 150
            const fudge = 5
            const start = Date.now()
            O.sleep(delay)
            const time = Date.now() - start
            expect(time).toBeLessThan(delay + fudge)
            expect(time).toBeGreaterThan(delay - fudge)
        })

        test("sleeping for zero milliseconds takes zero milliseconds", () => {
            const fudge = 5
            const start = Date.now()
            O.sleep(0)
            const time = Date.now() - start
            expect(time).toBeGreaterThanOrEqual(0)
            expect(time).toBeLessThanOrEqual(1)
        })

        test("interprets a negative duration as zero", () => {
            const fudge = 5
            const start = Date.now()
            O.sleep(-1000)
            const time = Date.now() - start
            expect(time).toBeGreaterThanOrEqual(0)
            expect(time).toBeLessThanOrEqual(1)
        })
    })
})