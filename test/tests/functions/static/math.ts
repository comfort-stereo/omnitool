import { O } from "../../../../source/omnitool"
import { errorTag } from "../../../utilities"

describe("math functions", () => {
    describe("abs", () => {
        test("returns the absolute value of a number", () => {
            expect(O.abs(2)).toEqual(2)
            expect(O.abs(-2)).toEqual(2)
            expect(O.abs(0)).toEqual(0)
        })
    })

    describe("ceil", () => {
        test("rounds a number up to the nearest integer", () => {
            expect(O.ceil(1.1)).toEqual(2)
        })

        test("the number can be negative", () => {
            expect(O.ceil(-1.1)).toEqual(-1)
        })

        test("does not affect integers", () => {
            expect(O.ceil(1)).toEqual(1)
        })
    })

    describe("clamp", () => {
        test("clamps a number between two other numbers", () => {
            expect(O.clamp(1, 2, 5)).toEqual(2)
            expect(O.clamp(5, -5, 0)).toEqual(0)
            expect(O.clamp(1, -5, 5)).toEqual(1)
        })

        test("works the same if the bounds are swapped", () => {
            expect(O.clamp(1, 5, 2)).toEqual(2)
            expect(O.clamp(5, 0, -5)).toEqual(0)
            expect(O.clamp(1, 5, -5)).toEqual(1)
        })
    })

    describe("floor", () => {
        test("rounds a number down to the nearest integer", () => {
            expect(O.floor(1.1)).toEqual(1)
        })

        test("the number can be negative", () => {
            expect(O.floor(-1.1)).toEqual(-2)
        })

        test("does not affect integers", () => {
            expect(O.floor(1)).toEqual(1)
        })
    })

    describe("gcd", () => {
        test("returns the gcd of two numbers", () => {
            expect(O.gcd(100, 5)).toEqual(5)
            expect(O.gcd(35, 25)).toEqual(5)
        })

        test("floors input floats to integers", () => {
            expect(O.gcd(0, 25.8)).toEqual(1)
            expect(O.gcd(25.8, 0)).toEqual(1)
            expect(O.gcd(0.8, 0.2)).toEqual(1)
        })

        test("returns one if either of the inputs is zero", () => {
            expect(O.gcd(0, 25)).toEqual(1)
            expect(O.gcd(25, 0)).toEqual(1)
            expect(O.gcd(0, 0)).toEqual(1)
        })

        test("always returns a positive integer when passed any negatives", () => {
            expect(O.gcd(100, -5)).toEqual(5)
            expect(O.gcd(35, -25)).toEqual(5)
            expect(O.gcd(-35, -25)).toEqual(5)
        })
    })

    describe("high", () => {
        describe("without transform", () => {
            test("returns the maximum of two numbers", () => {
                expect(O.high(10, -10)).toEqual(10)
                expect(O.high(-5, 5)).toEqual(5)
                expect(O.high(5, 10)).toEqual(10)
                expect(O.high(10, 5)).toEqual(10)
                expect(O.high(-5, -10)).toEqual(-5)
            })
        })

        describe("with transform", () => {
            const first = [1, 2, 3, 4]
            const second = [1, 2]
            test("returns the value for which a transform returns the maximum number", () => {
                expect(O.high(first, second, (array) => array.length)).toBe(first)
                expect(O.high(second, first, (array) => array.length)).toBe(first)
                expect(O.high(first, second, (array) => -array.length)).toBe(second)
                expect(O.high(second, first, (array) => -array.length)).toBe(second)
            })
        })
    })

    describe("low", () => {
        describe("without transform", () => {
            test("returns the minimum of two numbers", () => {
                expect(O.low(10, -10)).toEqual(-10)
                expect(O.low(-5, 5)).toEqual(-5)
                expect(O.low(5, 10)).toEqual(5)
                expect(O.low(10, 5)).toEqual(5)
                expect(O.low(-5, -10)).toEqual(-10)
            })
        })

        describe("with transform", () => {
            const first = [1, 2, 3, 4]
            const second = [1, 2]
            test("returns the value for which a transform returns the maximum number", () => {
                expect(O.low(first, second, (array) => array.length)).toBe(second)
                expect(O.low(second, first, (array) => array.length)).toBe(second)
                expect(O.low(first, second, (array) => -array.length)).toBe(first)
                expect(O.low(second, first, (array) => -array.length)).toBe(first)
            })
        })
    })

    describe("round", () => {
        test("rounds a number to the nearest integer", () => {
            expect(O.round(1.75)).toEqual(2)
            expect(O.round(1.5)).toEqual(2)
            expect(O.round(1.25)).toEqual(1)
        })

        test("can round to a given number of decimal places", () => {
            expect(O.round(1.755, 2)).toBeCloseTo(1.76, 4)
            expect(O.round(1.755555, 4)).toBeCloseTo(1.7556, 4)
            expect(O.round(1255, -2)).toBeCloseTo(1300, 4)
        })
    })
})