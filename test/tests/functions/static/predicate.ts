import { Predicate } from "../../../../source/types/types"
import { O } from "../../../../source/omnitool"

describe("predicate functions", () => {
    const types = {
        UNDEFINED: undefined,
        NULL: null,

        FALSE: false,
        TRUE: true,
        TRUE_WRAPPED: new Boolean(true),
        FALSE_WRAPPED: new Boolean(false),

        ZERO: 0,
        ONE: 1,
        INFINITY: Infinity,
        NAN: 1 / 0,
        ZERO_WRAPPED: new Number(0),
        ONE_WRAPPED: new Number(1),

        EMPTY_STRING: "",
        STRING: "string",
        EMPTY_STRING_WRAPPED: new String(""),
        STRING_WRAPPED: new String("string"),

        EMPTY_ARRAY: [],
        ARRAY: [1],

        EMPTY_OBJECT: {},
        OBJECT: {key: 1},

        FUNCTION: function () {
        },
        LAMBDA: () => {
        },

        ERROR: new Error(),
        REGEX: new RegExp(".*"),
        REGEX_LITERAL: /.*/,

        SYMBOL: Symbol.iterator
    }

    const keys = Object.keys(types)
    const values: any[] = []
    for (const key of keys) {
        values.push(types[key])
    }

    const asAny = (value: any): any => value

    const run = (predicate: Predicate<any>): any[] => {
        const matches: any[] = []
        for (const key of keys) {
            const value = types[key]
            if (predicate(value)) {
                matches.push(value)
            }
        }
        return matches
    }

    describe("exists", () => {
        test("returns true if the value is not null or undefined", () => {
            expect(run(O.exists)).toEqual(values.filter((element) => element !== undefined && element !== null))
        })

        test("type guard", () => {
            const value: number[] | null | undefined = asAny(0)
            if (O.exists(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isArray", () => {
        test("returns true if the value is an array", () => {
            expect(run(O.isArray)).toEqual([
                types.EMPTY_ARRAY,
                types.ARRAY
            ])
        })

        test("type guard", () => {
            const value: number[] | number = asAny(0)
            if (O.isArray(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isBoolean", () => {
        test("returns true if the value is a boolean primitive", () => {
            expect(run(O.isBoolean)).toEqual([
                types.FALSE,
                types.TRUE
            ])
        })

        test("type guard", () => {
            const value: boolean[] | boolean = asAny([])
            if (!O.isBoolean(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isDefined", () => {
        test("returns true if the value is defined", () => {
            expect(run(O.isDefined)).toEqual(values.filter((element) => element !== undefined))
        })

        test("type guard", () => {
            const value: boolean[] | undefined = asAny([])
            if (O.isDefined(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isError", () => {
        test("returns true if the value is an error", () => {
            expect(run(O.isError)).toEqual([types.ERROR])
        })

        test("type guard", () => {
            const value: boolean[] | Error = asAny(new Error())
            if (O.isError(value)) {
                O.noop(value.message)
            }
        })
    })

    describe("isFunction", () => {
        test("returns true if the value is an function", () => {
            expect(run(O.isFunction)).toEqual([
                types.FUNCTION,
                types.LAMBDA
            ])
        })

        test("type guard", () => {
            const value: boolean[] | Function = asAny(O.noop)
            if (O.isFunction(value)) {
                value()
            }
        })
    })

    describe("isNull", () => {
        test("returns true if the value is null", () => {
            expect(run(O.isNull)).toEqual([types.NULL])
        })

        test("type guard", () => {
            const value: boolean[] | null = asAny([])
            if (!O.isNull(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isNumber", () => {
        test("returns true if the value is a number primitive", () => {
            expect(run(O.isNumber)).toEqual([
                types.ZERO,
                types.ONE,
                types.INFINITY,
                types.NAN
            ])
        })

        test("type guard", () => {
            const value: number[] | number = asAny(1)
            if (O.isNumber(value)) {
                O.noop(value + 1)
            }
        })
    })

    describe("isntNull", () => {
        test("returns true if the value is not null", () => {
            expect(run(O.isntNull)).toEqual(values.filter((element) => element !== null))
        })

        test("type guard", () => {
            const value: boolean[] | null = asAny([])
            if (O.isntNull(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isObject", () => {
        test("returns true if the value is an object", () => {
            expect(run(O.isObject)).toEqual([
                types.TRUE_WRAPPED,
                types.FALSE_WRAPPED,
                types.ZERO_WRAPPED,
                types.ONE_WRAPPED,
                types.EMPTY_STRING_WRAPPED,
                types.STRING_WRAPPED,
                types.EMPTY_ARRAY,
                types.ARRAY,
                types.EMPTY_OBJECT,
                types.OBJECT,
                types.FUNCTION,
                types.LAMBDA,
                types.ERROR,
                types.REGEX,
                types.REGEX_LITERAL
            ])
        })

        test("type guard", () => {
            const value: object | null = asAny({key: 0})
            if (O.isObject(value)) {
                O.noop(value["key"])
            }
        })
    })

    describe("isPrimitive", () => {
        test("returns true if the value is a primitive", () => {
            expect(run(O.isPrimitive)).toEqual([
                types.UNDEFINED,
                types.NULL,
                types.FALSE,
                types.TRUE,
                types.ZERO,
                types.ONE,
                types.INFINITY,
                types.NAN,
                types.EMPTY_STRING,
                types.STRING,
                types.SYMBOL
            ])
        })

        test("type guard", () => {
            const value: number[] | number = asAny(0)
            if (O.isPrimitive(value)) {
                O.noop(value + 1)
            }
        })
    })

    describe("isRegularExpression", () => {
        test("returns true if the value is a regular expression object", () => {
            expect(run(O.isRegularExpression)).toEqual([
                types.REGEX,
                types.REGEX_LITERAL
            ])
        })

        test("type guard", () => {
            const value: RegExp | null = asAny(new RegExp(".*"))
            if (O.isRegularExpression(value)) {
                O.noop(value.test(""))
            }
        })
    })

    describe("isString", () => {
        test("returns true if the value is a string primitive", () => {
            expect(run(O.isString)).toEqual([
                types.EMPTY_STRING,
                types.STRING
            ])
        })

        test("type guard", () => {
            const value: string | null = asAny("")
            if (O.isString(value)) {
                O.noop(value.toUpperCase())
            }
        })
    })

    describe("isSymbol", () => {
        test("returns true if the value is a symbol primitive", () => {
            expect(run(O.isSymbol)).toEqual([types.SYMBOL])
        })

        test("type guard", () => {
            const value: number[] | Symbol = asAny(Symbol.iterator)
            if (!O.isSymbol(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("isUndefined", () => {
        test("returns true if the value is undefined", () => {
            expect(run(O.isUndefined)).toEqual([types.UNDEFINED])
        })

        test("type guard", () => {
            const value: boolean[] | undefined = asAny([])
            if (!O.isUndefined(value)) {
                O.noop(value.length)
            }
        })
    })

    describe("missing", () => {
        test("returns true if the value is not null or undefined", () => {
            expect(run(O.missing)).toEqual([
                types.UNDEFINED,
                types.NULL
            ])
        })

        test("type guard", () => {
            const value: number[] | null | undefined = asAny(0)
            if (!O.missing(value)) {
                O.noop(value.length)
            }
        })
    })
})