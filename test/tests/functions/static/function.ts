import { O } from "../../../../source/omnitool"

describe("function functions", () => {
    describe("apply", () => {
        test("applies an action to a value and returns the original value", () => {
            const array: number[] = [0]
            const result = O.apply(array, (array) => {
                array.push(1, 2, 3)
            })
            expect(array).toEqual([0, 1, 2, 3])
        })
    })

    describe("identity", () => {
        test("returns the input value with no modifications", () => {
            const array = [1, 2, 3]
            expect(O.identity(array)).toBe(array)
            expect(array).toEqual([1, 2, 3])
        })
    })

    describe("negate", () => {
        const animal = "hawk"

        test("returns a wrapper around an input function with a number result that negates the result", () => {
            const a = (value: string) => value.length
            const b = (value: string) => -value.length
            expect(O.negate(a)(animal)).toEqual(-4)
            expect(O.negate(b)(animal)).toEqual(4)
        })
    })

    describe("no", () => {
        test("returns false", () => {
            expect(O.no()).toEqual(false)
        })
    })

    describe("noop", () => {
        test("takes an arbitrary number of arguments of any type and returns undefined", () => {
            expect(O.noop()).toEqual(undefined)
            expect(O.noop(0, 1, [], {})).toEqual(undefined)
        })
    })

    describe("not", () => {
        const short = "hawk"
        const long = "giraffe"

        test("returns a wrapper around an input function with a boolean result that returns the opposite result", () => {
            const fn = (value: string) => value.length > 6
            expect(O.not(fn)(short)).toEqual(true)
            expect(O.not(fn)(long)).toEqual(false)
        })
    })

    describe("run", () => {
        test("immediately invokes a block callback and returns the result", () => {
            let count = 0
            const result = O.run(() => ++count)
            expect(count).toEqual(1)
            expect(result).toEqual(1)
        })
    })

    describe("times", () => {
        test("runs a callback function a given number of times", () => {
            let count = 0
            O.times(10, () => {
                count++
            })
            expect(count).toEqual(10)
        })

        test("does not run the callback if the count is zero or negative", () => {
            let count = 0
            O.times(0, () => {
                count++
            })
            O.times(-10, () => {
                count++
            })
            expect(count).toEqual(0)
        })

        test("the callback is passed proper indices", () => {
            let indices: number[] = []
            O.times(5, (i) => {
                indices.push(i)
            })
            expect(indices).toEqual([0, 1, 2, 3, 4])
        })

        test("the loop can be exited if the done() function passed to the callback is invoked", () => {
            const count = 0
            let indices: number[] = []
            O.times(20, (i, done) => {
                indices.push(i)
                if (i === 4) {
                    done()
                }
            })
            expect(indices).toEqual([0, 1, 2, 3, 4])
        })
    })

    describe("yes", () => {
        test("returns true", () => {
            expect(O.yes()).toEqual(true)
        })
    })
})
