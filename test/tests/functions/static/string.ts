import { O } from "../../../../source/omnitool"

describe("string functions", () => {
    const lowercaseAlpha = "abcdefghijklmnopqrstuvwxyz"
    const uppercaseAlpha = lowercaseAlpha.toUpperCase()
    const alpha = lowercaseAlpha + uppercaseAlpha
    const digits = "0123456789"
    const symbols = "!@#$%^&*()~`-_+='\".>,<|\\"
    const whitespace = " \t\n\r"

    describe("isAlpha", () => {
        test("returns true if all characters are alphabetical", () => {
            expect(O.isAlpha(alpha)).toEqual(true)
        })

        test("returns false otherwise", () => {
            expect(O.isAlpha("This has spaces" + alpha)).toEqual(false)
            expect(O.isAlpha(alpha + symbols)).toEqual(false)
            expect(O.isAlpha(alpha + digits)).toEqual(false)
            expect(O.isAlpha(alpha + symbols + digits)).toEqual(false)
        })

        test("returns false if there are no characters", () => {
            expect(O.isAlpha("")).toEqual(false)
        })
    })

    describe("isDigit", () => {
        test("returns true if all characters are numerical digits", () => {
            expect(O.isDigit(digits)).toEqual(true)
        })

        test("returns false otherwise", () => {
            expect(O.isDigit(digits + symbols)).toEqual(false)
            expect(O.isDigit(digits + alpha)).toEqual(false)
            expect(O.isDigit("-" + digits)).toEqual(false)
            expect(O.isDigit("1.2")).toEqual(false)
            expect(O.isDigit("-1.2")).toEqual(false)
        })

        test("returns false if there are no characters", () => {
            expect(O.isDigit("")).toEqual(false)
        })
    })

    describe("isLower", () => {
        test("returns true if all alphabetic characters in the string are lowercase", () => {
            expect(O.isLower(lowercaseAlpha)).toEqual(true)
            expect(O.isLower(lowercaseAlpha + symbols)).toEqual(true)
            expect(O.isLower(lowercaseAlpha + digits)).toEqual(true)
            expect(O.isLower(lowercaseAlpha + symbols + digits)).toEqual(true)
        })

        test("returns false otherwise", () => {
            expect(O.isLower(alpha)).toEqual(false)
            expect(O.isLower(uppercaseAlpha + symbols)).toEqual(false)
            expect(O.isLower(uppercaseAlpha + digits)).toEqual(false)
            expect(O.isLower(uppercaseAlpha + symbols + digits)).toEqual(false)
        })

        test("returns false if there are no characters", () => {
            expect(O.isLower("")).toEqual(false)
        })
    })

    describe("isUpper", () => {
        test("returns true if all alphabetic characters in the string are uppercase", () => {
            expect(O.isUpper(uppercaseAlpha)).toEqual(true)
            expect(O.isUpper(uppercaseAlpha + symbols)).toEqual(true)
            expect(O.isUpper(uppercaseAlpha + digits)).toEqual(true)
            expect(O.isUpper(uppercaseAlpha + symbols + digits)).toEqual(true)
        })

        test("returns false otherwise", () => {
            expect(O.isUpper(alpha)).toEqual(false)
            expect(O.isUpper(lowercaseAlpha + symbols)).toEqual(false)
            expect(O.isUpper(lowercaseAlpha + digits)).toEqual(false)
            expect(O.isUpper(lowercaseAlpha + symbols + digits)).toEqual(false)
        })

        test("returns false if there are no characters", () => {
            expect(O.isUpper("")).toEqual(false)
        })
    })

    describe("isBlank", () => {
        test("returns true if all characters are whitespace or there are no characters", () => {
            expect(O.isBlank(whitespace)).toEqual(true)
            expect(O.isBlank(" ")).toEqual(true)
            expect(O.isBlank("")).toEqual(true)
        })

        test("returns false otherwise", () => {
            expect(O.isBlank(whitespace + alpha)).toEqual(false)
            expect(O.isBlank(whitespace + digits)).toEqual(false)
            expect(O.isBlank(whitespace + symbols)).toEqual(false)
            expect(O.isBlank(whitespace + alpha + symbols + digits)).toEqual(false)
        })
    })

    describe("lower", () => {
        test("converts all alphabetical characters to lower case", () => {
            expect(O.lower("@#Hi TheRe!+")).toEqual("@#hi there!+")
        })

        test("handles empty", () => {
            expect(O.lower("")).toEqual("")
        })
    })

    describe("pad", () => {
        const animal = "hawk"

        test("centers an input string in a field of characters of given width", () => {
            expect(O.pad(animal, 10, "_+")).toEqual("_+_hawk_+_")
            expect(O.pad(animal, 9, "_+")).toEqual("_+_hawk_+")
        })

        test("uses spaces by default", () => {
            expect(O.pad(animal, 10)).toEqual("   hawk   ")
            expect(O.pad(animal, 9)).toEqual("   hawk  ")
        })

        test("returns the original string if the field width is less than the strings's length", () => {
            expect(O.pad(animal, 2, "_+")).toEqual("hawk")
            expect(O.pad(animal, 2)).toEqual("hawk")
        })

        test("returns the original string if the padding string is empty", () => {
            expect(O.pad(animal, 100, "")).toEqual("hawk")
        })
    })

    describe("padStart", () => {
        const animal = "hawk"

        test("adds padding characters to the start of a string until it has a specified width", () => {
            expect(O.padStart(animal, 10, "_+")).toEqual("_+_+_+hawk")
            expect(O.padStart(animal, 9, "_+")).toEqual("_+_+_hawk")
        })

        test("uses spaces by default", () => {
            expect(O.padStart(animal, 10)).toEqual("      hawk")
            expect(O.padStart(animal, 9)).toEqual("     hawk")
        })

        test("returns the original string if the field width is less than the strings's length", () => {
            expect(O.padStart(animal, 2, "_+")).toEqual("hawk")
            expect(O.padStart(animal, 2)).toEqual("hawk")
        })

        test("returns the original string if the padding string is empty", () => {
            expect(O.padStart(animal, 100, "")).toEqual("hawk")
        })
    })

    describe("padEnd", () => {
        const animal = "hawk"

        test("adds padding characters to the start of a string until it has a specified width", () => {
            expect(O.padEnd(animal, 10, "_+")).toEqual("hawk_+_+_+")
            expect(O.padEnd(animal, 9, "_+")).toEqual("hawk_+_+_")
        })

        test("uses spaces by default", () => {
            expect(O.padEnd(animal, 10)).toEqual("hawk      ")
            expect(O.padEnd(animal, 9)).toEqual("hawk     ")
        })

        test("returns the original string if the field width is less than the strings's length", () => {
            expect(O.padEnd(animal, 2, "_+")).toEqual("hawk")
            expect(O.padEnd(animal, 2)).toEqual("hawk")
        })

        test("returns the original string if the padding string is empty", () => {
            expect(O.padEnd(animal, 100, "")).toEqual("hawk")
        })
    })

    describe("upper", () => {
        test("converts all alphabetical characters to upper case", () => {
            expect(O.upper("@#Hi TheRe!+")).toEqual("@#HI THERE!+")
        })

        test("handles empty", () => {
            expect(O.upper("")).toEqual("")
        })
    })
})