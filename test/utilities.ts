import { Pipe } from "../source/types/sequence/pipe"
import { Sequence } from "../source/types/types"

/**
 * Checks that the values returned by both the pipe and span versions of an extracted method are the same.
 */
export function checkResult<T extends Function>(sequenceFunction: T, elementsOnly: boolean = false): T {

    function checked(sequence: Sequence<T>, ...args: any[]) {
        let positionalResult, iteratorResult
        let positionalError: Error | null = null
        let iteratorError: Error | null = null

        try {
            positionalResult = sequenceFunction([...sequence], ...args)
            if (elementsOnly) {
                positionalResult = [...positionalResult]
            }
        } catch (error) {
            positionalError = error
        }

        try {
            iteratorResult = sequenceFunction(new Pipe(sequence), ...args)
            if (elementsOnly) {
                iteratorResult = [...iteratorResult]
            }
        } catch (error) {
            iteratorError = error
        }

        try {
            expect(positionalError).toEqual(iteratorError)
        } catch (error) {
            const positionalMessage = positionalError ? "THROWN" : "NONE THROWN"
            const iteratorMessage = iteratorError ? "THROWN" : "NONE THROWN"
            throw new Error("Errors did not match: (" + positionalMessage + ") & (" + iteratorMessage + ")")
        }

        if (positionalError) {
            throw positionalError
        }

        expect(positionalResult).toEqual(iteratorResult)
        return positionalResult
    }

    return checked as any
}

/**
 * Checks that the sequence of elements returned by both the pipe and span versions of an extracted method return the
 * same elements or throw the same errors.
 */
export function checkSequenceResult<T extends Function>(sequenceFunction: T): T {
    return checkResult(sequenceFunction, true)
}

/**
 * Returns true if the elements of a sequence are equal to an array of other elements, regardless of the actual
 * container.
 */
export function expectElements<T>(sequence: Sequence<T>, elements: T[]) {
    expect(Array.from(sequence)).toEqual(elements)
}

/**
 * Returns the error tag for a function. This tag should always be included in errors thrown by the function. Used
 * for checking thrown errors.
 */
export function errorTag(functionName: string): string {
    return "[" + functionName + "()]:"
}